<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Modules\UserModule\Entities\UserServer;

class ExpiryDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:expiry_date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->toDateString();
        $user_servers = UserServer::where('expiry_date', '>=', $now)->get();
        if ($user_servers != null) {
            foreach ($user_servers as $server) {
                $server->update(['expired' => 1]);

                Log::info('server is updated');

            }
        }


    }
}
