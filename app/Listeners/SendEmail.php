<?php

namespace App\Listeners;

class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */


    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(\App\Events\SendEmail $email)
    {
        \App\Jobs\SendEmail::dispatch($email->data);

    }
}
