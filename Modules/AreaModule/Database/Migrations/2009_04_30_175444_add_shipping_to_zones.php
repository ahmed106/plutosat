<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingToZones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zones', function (Blueprint $table) {

          $table->integer('status')->default(1);
          $table->integer('sub_total_limit_type')->default(1);
          $table->integer('sub_total_limit')->default(0);
          $table->date('available_shipping_date')->nullable();
          $table->integer('shipping_type')->default(1);
          $table->integer('shipping_quota')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
