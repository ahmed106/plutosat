<?php

namespace Modules\AreaModule\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\AreaModule\Entities\Country;

class AreaModuleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $countries = new Country();
        $countries->code = '+20';
        $countries->phone_code = '+20';
        $countries->save();
        $countries->translation()->create([
            'name' => 'egypt',
            'country_id' => $countries->id,
            'locale' => 'ar'
        ]);
        $countries->translation()->create([
            'name' => 'egypt',
            'country_id' => $countries->id,
            'locale' => 'en'
        ]);
    }
}
