<?php

namespace Modules\AreaModule\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\FacultyModule\Entities\University;
use Modules\InstituteModule\Entities\Institute;

class Government extends Model
{
    use Translatable;

    protected $fillable = ['country_id'];

    protected $table = "governments";
    public $translatedAttributes = ['name', 'government_id'];
    public $translationModel = GovernmentTranslation::class;

    function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function universities()
    {
        return $this->hasMany(University::class, 'government_id');
    }//end function

    public function institutes()
    {
        return $this->hasMany(Institute::class, 'government_id');
    }//end function

}
