<?php

namespace Modules\AreaModule\Entities;

use Illuminate\Database\Eloquent\Model;

class CountryTranslations extends Model
{
    public $timestamps = false;
    protected $table = 'countries_translations';
    protected $fillable = ['name'];
}
