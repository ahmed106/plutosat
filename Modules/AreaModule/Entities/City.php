<?php

namespace Modules\AreaModule\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use Translatable;

    protected $fillable = ['government_id', 'country_id', 'type'];

    protected $table = "cities";
    public $translatedAttributes = ['name'];
    public $translationModel = CityTranslation::class;

    function government()
    {
        return $this->belongsTo(Government::class, 'government_id');
    }

    function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
