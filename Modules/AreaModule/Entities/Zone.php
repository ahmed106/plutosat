<?php

namespace Modules\AreaModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class Zone extends Model
{
    use Translatable;

    protected $fillable = ['government_id', 'country_id','city_id','shipping_type','shipping_quota','sub_total_limit','sub_total_limit_type','storeLocation'];

    protected $table = "zones";
    public $translatedAttributes = ['name'];
    public $translationModel = ZoneTranslation::class;
    function government(){
        return $this->belongsTo(Government::class,'government_id');
    }
    function country(){
        return $this->belongsTo(Country::class,'country_id');
    }
    function city(){
        return $this->belongsTo(City::class,'city_id');
    }
}
