<?php

namespace Modules\AreaModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AreaModule\Repository\CountryRepository;
use Modules\CommonModule\Helper\UploaderHelper;

class CountryController extends Controller
{
    use UploaderHelper;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $countries = CountryRepository::findAll();

        return view('areamodule::Country.index', ['countries' => $countries]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('areamodule::Country.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $countryData = $request->except('_token');

        CountryRepository::save($countryData);

        return redirect('admin-panel/country')->with('success', 'success');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $country = CountryRepository::find($id);

        return view('areamodule::Country.edit', ['country' => $country]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $country = $request->except('_method', '_token');
        $countryTrans = $request->only('ar', 'en', 'fr');

        CountryRepository::update($id, $country, $countryTrans);

        return redirect('admin-panel/country')->with('updated', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $country = CountryRepository::find($id);
        CountryRepository::delete($country);

        return redirect()->back();
    }
}
