<?php

namespace Modules\AreaModule\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AreaModule\Repository\CityRepository;
use Modules\AreaModule\Repository\CountryRepository;
use Modules\AreaModule\Repository\GovernmentRepository;
use Modules\AreaModule\Repository\ZoneRepository;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $zone = ZoneRepository::findAll();

        return view('areamodule::Zone.index', ['zone' => $zone]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $country = CountryRepository::findAll();
        return view('areamodule::Zone.create', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $zoneData = $request->except('_token');
        ZoneRepository::save($zoneData);

        return redirect('admin-panel/zone')->with('success', 'success');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $country = CountryRepository::findAll();
        $zone = ZoneRepository::find($id);
        $government = GovernmentRepository::findWhere('country_id', $zone->country_id);
        $city = CityRepository::findWhere('government_id', $zone->government_id);

        return view('areamodule::Zone.edit', ['zone' => $zone, 'country' => $country, 'government' => $government, 'city' => $city]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $zone = $request->except('_method', '_token');
        $zoneTrans = $request->only('ar', 'en', 'fr');

        ZoneRepository::update($id, $zone, $zoneTrans);

        return redirect('admin-panel/zone')->with('updated', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $zone = ZoneRepository::find($id);
        ZoneRepository::delete($zone);

        return redirect()->back();
    }

    public function getZone($gov_id)
    {
        $city = CityRepository::findWhere('government_id', $gov_id);
        return response()->json($city);
    }

    public function getZones($city_id)
    {
        $zones = ZoneRepository::findZonesWhere('city_id', $city_id);
        return response()->json($zones);
    }

}
