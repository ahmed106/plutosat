<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin-panel')->group(function () {
    Route::resource('/servers', 'ServersController')->except('show');
    Route::resource('/free-servers', 'FreeServersController')->except('show');
});
