@extends('commonmodule::layouts.master')

@section('title')
    Servers
@endsection

@section('css')

@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            Servers
        </h1>

    </section>
@endsection

@section('content')


    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> Servers</h3>
            </div>


            @if ($errors->any())
                <div class="alert alert-danger ">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif

            <form class="form-horizontal" action="{{route('servers.store')}}" method="POST">
                {{ csrf_field() }}

                <div class="box-body">
                    <div class="col-md-9">
                        <div class="nav-tabs-custom">


                            <div class="tab-content">


                                <div class="tab-pane active" id="">


                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Host :</label>
                                        <div class="col-sm-9">
                                            <input required type="text" autocomplete="off" value="" class="form-control" name="host">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Port :</label>
                                        <div class="col-sm-9">
                                            <input required type="text" autocomplete="off" value="" class="form-control" name="port">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">User Name :</label>
                                        <div class="col-sm-9">
                                            <input required type="text" autocomplete="off" value="" class="form-control" name="username">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Password :</label>
                                        <div class="col-sm-9">
                                            <input required type="text" autocomplete="off" value="" class="form-control" name="password">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Notes :</label>
                                        <div class="col-sm-9">
                                            <textarea name="notes" class="textarea" id="editor" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>


                                </div>

                            </div>


                        </div>
                    </div>


                </div>
                <div class=" box-footer">
                    <button type="submit" class="btn btn-primary pull-right">
                        Save
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>

    </section>
@endsection

@section('javascript')
    <script src="{{adminurl('bower_components/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{adminurl('bower_components/ckeditor/ckfinder/ckfinder.js')}}"></script>
    @include('commonmodule::includes.swal')
    <script>
        CKFinder.setupCKEditor();

        $(function () {
            CKEDITOR.replace('editor');
        });
    </script>

@endsection
