@extends('commonmodule::layouts.master')

@section('title')
    Servers
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content-header')
    <section class="content-header">
        <h1> Servers </h1>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Servers</h3>
                        {{-- Add New--}}
                        <a href="{{url('admin-panel/servers/create')}}" type="button" class="btn btn-success pull-right">
                            <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{ trans('adminmodule::admin.add_new') }}
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="adminsTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Host</th>
                                <th>Port</th>
                                <th>Username</th>
                                <th>Password</th>
                                <th>Expiry Date</th>
                                <th>Notes</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($servers as $server)

                                <tr>
                                    <td> {{$server->id}} </td>

                                    <td> {{$server->host}} </td>

                                    <td> {{$server->port}} </td>
                                    <td> {{$server->username}} </td>
                                    <td> {{$server->password}} </td>
                                    <td> {{$server->user_server?$server->user_server->expiry_date:''}} </td>
                                    <td> {{strip_tags($server->notes)}} </td>

                                    <td>

                                        <a title="Edit" href="{{url('admin-panel/servers/' . $server->id . '/edit')}}" type="button" class="btn btn-primary">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>

                                        <form class="inline" action="{{url('admin-panel/servers/' . $server->id)}}" method="POST">
                                            {{ method_field('DELETE') }} {!! csrf_field() !!}
                                            <button title="Delete" type="submit" onclick="return confirm('Are you sure, You want to delete Admin Data?')" type="button" class="btn btn-danger">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('javascript') {{-- sweet alert --}}

@include('commonmodule::includes.swal')

<!-- DataTables -->
<script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#adminsTable').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'language': {!! yajra_lang() !!}
        });
    })

</script>
@endsection
