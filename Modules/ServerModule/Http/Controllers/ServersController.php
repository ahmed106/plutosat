<?php

namespace Modules\ServerModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\ServerModule\Entities\Server;
use Modules\UserModule\Entities\UserServer;

class ServersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }//end function


    public function index()
    {

        $servers = Server::with('user_server')->get();


        return view('servermodule::servers.index', compact('servers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('servermodule::servers.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'username' => [Rule::unique('servers', 'username')],
            'password' => ['required'],
            'port' => 'required',
            'host' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());

        }
        if (!$request->has('is_booked')) {

            $request->request->add(['is_booked' => 0]);


        }
        $data = $request->except(['_token']);


        Server::create($data);
        return redirect()->route('servers.index')->with('success', 'data stored successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('servermodule::servers.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $server = Server::findOrFail($id);
        return view('servermodule::servers.edit', compact('server'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        if (!$request->has('is_booked')) {
            $request->request->add(['is_booked' => 0]);
            $userServer = UserServer::where('server_id', $id)->first();
            if ($userServer) {
                $userServer->delete();

            }


        }
        $data = $request->except(['_method', '_token']);

        $server = Server::findOrFail($id);

        $validator = Validator::make($data, [

            'username' => [Rule::unique('servers', 'username')->ignore($server->username, 'username')],
            'port' => 'required',
            'host' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());

        }


        $server->update($data);
        return redirect()->route('servers.index')->with('updated', 'data updated');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $server = DB::table('servers')->where('id', $id)->delete();

        return redirect()->route('servers.index')->with('deleted', 'data deleted');


    }
}
