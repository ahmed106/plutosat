<?php

namespace Modules\ServerModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\ServerModule\Entities\FreeServer;
use Modules\ServerModule\Entities\UserFreeServer;

class FreeServersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }//end function


    public function index()
    {
        $servers = DB::table('free_servers')->get();

        return view('servermodule::free_servers.index', compact('servers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('servermodule::free_servers.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [

            'username' => [Rule::unique('free_servers', 'username')],
            'password' => ['required'],
            'port' => 'required',
            'host' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());

        }

        $data = $request->except(['_token']);


        FreeServer::create($data);
        return redirect()->route('free-servers.index')->with('success', 'data stored successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('servermodule::free-servers.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $server = FreeServer::findOrFail($id);
        return view('servermodule::free_servers.edit', compact('server'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {


        $data = $request->except(['_method', '_token']);

        $server = FreeServer::findOrFail($id);

        $validator = Validator::make($data, [

            'username' => [Rule::unique('servers', 'username')->ignore($server->username, 'username')],
            'port' => 'required',
            'host' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());

        }


        $server->update($data);
        return redirect()->route('free-servers.index')->with('updated', 'data updated');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {


        UserFreeServer::where('free_server_id', $id)->delete();
        $server = DB::table('free_servers')->where('id', $id)->delete();

        return redirect()->route('free-servers.index')->with('deleted', 'data deleted');


    }
}
