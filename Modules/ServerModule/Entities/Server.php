<?php

namespace Modules\ServerModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\UserModule\Entities\UserServer;

class Server extends Model
{
    protected $guarded = [];

    public function servers()
    {
        return $this->hasMany(UserServer::class, 'server_id');

    }//end function

    public function user_server()
    {
        return $this->hasOne(UserServer::class, 'server_id');

    }//end function


}
