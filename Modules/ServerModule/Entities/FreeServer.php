<?php

namespace Modules\ServerModule\Entities;

use Illuminate\Database\Eloquent\Model;

class FreeServer extends Model
{
    protected $table = 'free_servers';
    protected $guarded = [];


    public function userServer()
    {
        return $this->hasOne(UserFreeServer::class);

    }//end function

}
