<?php

namespace Modules\ServerModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\UserModule\Entities\User;

class UserFreeServer extends Model
{
    protected $fillable = ['user_id', 'free_server_id'];

    protected $table = 'user_free_servers';

    public function server()
    {
        return $this->belongsTo(User::class, 'user_id');

    }//end function

    public function freeServer()
    {
        return $this->belongsTo(FreeServer::class, 'free_server_id');

    }//end function


}
