<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{__('commonmodule::sidebar.mainnav')}}</li>

            <!-- Common Module -->
            <li>
                <a href="{{url('/admin-panel')}}">
                    <i class="fa fa-home"></i> <span>{{__('commonmodule::sidebar.home')}} </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>

            <li>
                <a href="{{url('admin-panel/servers')}}">
                    <i class="fa fa-server"></i> <span>Servers </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>

            <li>
                <a href="{{url('admin-panel/free-servers')}}">
                    <i class="fa fa-server"></i> <span>Free Servers </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>

            <li>
                <a href="{{route('resellers.index')}}">
                    <i class="fa fa-send"></i> <span>Resellers </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>
            <li>
                <a href="{{url('admin-panel/users')}}">
                    <i class="fa fa-users"></i> <span>Users </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-globe" aria-hidden="true"></i> <span>Area</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Country -->
                    <li><a href="{{ url('admin-panel/country') }}"><i
                                class="fa fa-circle-o"></i>Country</a></li>

                    <!-- Government -->
                    <li><a href="{{ url('admin-panel/government') }}"><i
                                class="fa fa-circle-o"></i>governments</a></li>

                    <!-- city -->
                    <li><a href="{{ url('admin-panel/city') }}"><i
                                class="fa fa-circle-o"></i> City</a></li>
                    <!-- zone -->
                    <li><a href="{{ url('admin-panel/zone') }}"><i
                                class="fa fa-circle-o"></i>Zone</a></li>
                </ul>
            </li>


            @if(in_array('blog_app',$activeApps))

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        <span>{{__('commonmodule::sidebar.blog')}}</span>
                        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <!-- Categories -->
                        <li><a href="{{ url('admin-panel/blog-categories') }}"><i
                                    class="fa fa-circle-o"></i>{{__('commonmodule::sidebar.blogcateg')}}</a></li>

                        <!-- Blog -->
                        <li><a href="{{ url('admin-panel/blogs') }}"><i
                                    class="fa fa-circle-o"></i> {{__('commonmodule::sidebar.blog')}}</a></li>
                    </ul>
                </li>
            @endif

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tags"></i> <span>{{trans('commonmodule::sidebar.widgets')}}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Slider -->
                    <li><a href="{{ url('admin-panel/widgets/slider') }}"><i
                                class="fa fa-circle-o"></i> {{trans('commonmodule::sidebar.slider')}}</a>
                    </li>


                    <!-- Testimonial -->
                    <li><a href="{{ url('admin-panel/widgets/testimonials') }}"><i
                                class="fa fa-circle-o"></i> {{trans('commonmodule::sidebar.monial')}}</a></li>
                    <li><a href="{{ url('admin-panel/widgets/partners') }}"><i
                                class="fa fa-circle-o"></i> {{trans('commonmodule::sidebar.partner')}}</a></li>


                    {{--<!-- Team -->--}}
                    {{--                    <li><a href="{{ url('admin-panel/widgets/why_us') }}"><i--}}
                    {{--                                class="fa fa-circle-o"></i> Why Us </a></li>--}}

                    {{--                    <!-- contact -->--}}
                    {{--                    <li><a href="{{url('admin-panel/widgets/contact_us')}}"><i--}}
                    {{--                                class="fa fa-circle-o"></i> {{trans('commonmodule::sidebar.contact')}}</a></li>--}}


                    <li><a href="{{url('admin-panel/widgets/acheive')}}"><i
                                class="fa fa-circle-o"></i> {{trans('commonmodule::sidebar.acheive')}}</a></li>

                    <li><a href="{{url('admin-panel/widgets/page')}}"><i
                                class="fa fa-circle-o"></i> {{trans('commonmodule::sidebar.pages')}}</a>
                    </li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    <span>{{__('commonmodule::sidebar.config')}}</span>
                    <span class="pull-right-container">
                             <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    @foreach($config_categs as $categ)
                        <li><a href="{{ route('showconfig',$categ->id) }}"><i
                                    class="fa fa-circle-o"></i> {{ $categ->title}}</a>
                        </li>
                    @endforeach

                </ul>
            </li>


            @role('admin|superadmin|owner')
        <!-- <li>
                <a href="{{ url('admin-panel/config-module') }}">
                    <i class="fa fa-cog" aria-hidden="true"></i><span>{{__('commonmodule::sidebar.config')}} </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li> -->
            @endrole

            @role('superadmin|owner')
            <li>
                <a href="{{ url('admin-panel/admins') }}">
                    <i class="fa fa-user" aria-hidden="true"></i><span>{{__('commonmodule::sidebar.admins')}} </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>
            @endrole

            @role('owner')
            <li>
                <a href="{{ url('/admin-panel/commonmodule/activate-lang') }}">
                    <i class="fa fa-language" aria-hidden="true">
                    </i>
                    <span>
                        {{__('commonmodule::sidebar.langs')}}
                    </span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            @endrole

            @role('owner')
            <li>
                <a href="{{ url('admin-panel/commonmodule/settings/apps') }}">
                    <i class="fa fa-window-restore" aria-hidden="true"></i>
                    </i>
                    <span>
                        {{__('commonmodule::sidebar.apps')}}
                    </span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            @endrole
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
