@if(count($errors) > 0)

    <div class="alert alert-danger text-center ">

        @foreach($errors->all() as $error)

            <li style="list-style: none">{{$error}}</li>
        @endforeach
        {{--End Foreach--}}

    </div>
@endif
{{--End If--}}

@if(session()->has('error'))
    <div class="alert alert-danger text-center ">


        <li style="list-style: none">{{session('error')}}</li>


    </div>

@endif
{{--End If--}}
