<footer id="footer" class="mt-0">
    <div class="container my-4">
        <div class="row py-5">
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <a href="{{url('/')}}">
                    <img alt="Porto" width="200" height="100" data-sticky-width="150"
                         data-sticky-height="80" src="{{asset('images/logo.png')}}">
                </a>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <h5 class="text-5 text-transform-none font-weight-semibold text-color-light mb-4">@lang('frontmodule::home.opening_hours')</h5>
                <p class="text-4 mb-0">Mon-Fri: <span class="text-color-light">8:30 am to 5:00 pm</span></p>
                <p class="text-4 mb-0">Saturday: <span class="text-color-light">9:30 am to 1:00 pm</span></p>
                <p class="text-4 mb-0">Sunday: <span class="text-color-light">Closed</span></p>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <h5 class="text-5 text-transform-none font-weight-semibold text-color-light mb-4">@lang('frontmodule::home.contact_us')</h5>
                <p class="text-7 text-color-light font-weight-bold mb-2">{{$config['phone']}}</p>
                {{--                <p class="text-4 mb-0">Sales: <span class="text-color-light">(800) 123 4568</span></p>--}}
            </div>
            <div class="col-md-6 col-lg-3">

                <h5 class="text-5 text-transform-none font-weight-semibold text-color-light mb-4">@lang('frontmodule::home.social_media')</h5>
                <ul class="footer-social-icons social-icons m-0">
                    <li class="social-icons-facebook"><a href="{{$config['fb_link']}}" target="_blank" title="Facebook"><i class="fab fa-facebook-f text-2"></i></a></li>
                    <li class="social-icons-twitter"><a href="{{$config['tw_link']}}" target="_blank" title="Twitter"><i class="fab fa-twitter text-2"></i></a></li>
                    <li class="social-icons-linkedin"><a href="{{$config['telegram']}}" target="_blank" title="Telegram"><i class="fab fa-telegram text-2"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-copyright footer-copyright-style-2 pb-4">
            <div class="py-2">
                <div class="row py-4">
                    <div class="col d-flex align-items-center justify-content-center mb-4 mb-lg-0">
                        <p>© Copyright {{date('Y')}}. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
