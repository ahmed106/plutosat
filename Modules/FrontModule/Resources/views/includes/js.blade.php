<!-- Vendor -->
<script src="{{asset('assets/front')}}/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/popper/umd/popper.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/lazysizes/lazysizes.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/isotope/jquery.isotope.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/vide/jquery.vide.min.js"></script>
<script src="{{asset('assets/front')}}/vendor/vivus/vivus.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{asset('assets/front')}}/js/theme.js"></script>


<!-- Theme Custom -->
<script src="{{asset('assets/front')}}/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="{{asset('assets/front')}}/js/theme.init.js"></script>
<script src="{{asset('assets/front')}}/js/views/view.contact.js"></script>
<script src="{{asset('assets/front')}}/vendor/jquery.instagramfeed/jquery.instagramFeed.min.js"></script>
<script src="{{asset('assets/front')}}/js/examples/examples.instagramFeed.js"></script>
