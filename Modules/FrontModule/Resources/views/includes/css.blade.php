<!-- Favicon -->
<link rel="shortcut icon" href="{{asset('images/logo.png')}}" type="image/x-icon"/>
<link rel="apple-touch-icon" href="{{asset('assets/front')}}/img/apple-touch-icon.png">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">


<!-- Web Fonts  -->
<link id="googleFonts"
      href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light&display=swap"
      rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/animate/animate.compat.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/vendor/magnific-popup/magnific-popup.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="{{asset('assets/front')}}/css/theme.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/css/theme-elements.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/css/theme-blog.css">
<link rel="stylesheet" href="{{asset('assets/front')}}/css/theme-shop.css">


<!-- Skin CSS -->
<link id="skinCSS" rel="stylesheet" href="{{asset('assets/front')}}/css/skins/skin-corporate-5.css">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="{{asset('assets/front')}}/css/custom.css">

<!-- Head Libs -->
<script src="{{asset('assets/front')}}/vendor/modernizr/modernizr.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
<link href="http://fonts.cdnfonts.com/css/cairo-2" rel="stylesheet">

@if(app()->getLocale() =='ar')
    <link rel="stylesheet" href="{{asset('assets/front')}}/rtl/css/rtl-theme.css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/rtl/css/rtl-theme-elements.css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/rtl/rtl-css/theme-blog.css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/rtl/rtl-css/theme-shop.css">
    <style>

        body, h1, h2, h3, h4, h5, h6, span, div, p {
            font-family: "Cairo", Sans-Serif !important;
        }
    </style>
@endif
