<header id="header" class="header-effect-shrink"
        data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 120, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-top-0">
        <div class="header-top header-top-borders">
            <div class="container h-100">
                <div class="header-row h-100">
                    <div class="header-column justify-content-start">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">

                                    <li class="nav-item nav-item-borders py-2">
                                        <a href="tel:123-456-7890"><i
                                                class="fab fa-whatsapp text-4 text-color-primary"
                                                style="top: 0;"></i> {{$config['phone']}}</a>
                                    </li>
                                    <li class="nav-item nav-item-borders py-2 d-none d-md-inline-flex">
                                        <a href="mailto:mail@domain.com"><i
                                                class="far fa-envelope text-4 text-color-primary"
                                                style="top: 1px;"></i> {{$config['email']}}</a>
                                    </li>
                                </ul>

                            </nav>


                            <div class="header-column justify-content-end">
                                <div class="header-row">
                                    <nav class="header-nav-top">
                                        <ul class="nav nav-pills">

                                            <li class="nav-item nav-item-borders py-2 pr-0 dropdown">
                                                <a class="nav-link" href="#" role="button" id="dropdownLanguage"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-flag"></i>
                                                    @lang('frontmodule::home.lang')
                                                    <i class="fas fa-angle-down"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                                                    @foreach($activeLang as $lang)
                                                        <a class="dropdown-item" href="{{url('/'.$lang->lang)}}"><img src="" class="flag-icon flag-icon-{{$lang->lang=='en'?'us':$lang->lang}}"/> {{$lang->display_lang}}</a>
                                                    @endforeach
                                                    {{--End Foreach--}}
                                                </div>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{url('/')}}">
                                <img alt="Porto" width="150" height="80" data-sticky-width="150"
                                     data-sticky-height="80" src="{{asset('images/logo.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-stripe order-2 order-lg-1">
                            <div
                                class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle {{route_exists('home')}}" href="{{route('front.home')}}">
                                                @lang('frontmodule::home.home')
                                            </a>

                                        </li>
                                        <li class="dropdown dropdown-mega">


                                            <a class="dropdown-item dropdown-toggle {{route_exists('about')}} " href="{{route('front.about')}}">

                                                @lang('frontmodule::home.about')
                                            </a>

                                        </li>

                                        <li class="dropdown dropdown-mega">


                                            <a class="dropdown-item dropdown-toggle {{route_exists('technicians')}} " href="{{route('front.technicians')}}">

                                                @lang('frontmodule::home.technicians')
                                            </a>

                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle {{route_exists('blogs')}}  {{route_exists('blogs_show')}} " href="{{route('front.blogs')}}">

                                                @lang('frontmodule::home.blogs')
                                            </a>

                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle {{route_exists('freeServer')}}  " href="{{route('front.freeServer')}}">

                                                @lang('frontmodule::home.cccam')
                                            </a>

                                        </li>

                                        <li class="dropdown">
                                            <a class="dropdown-item dropdown-toggle {{route_exists('contact_us')}}" href="{{route('front.contact_us')}}">

                                                @lang('frontmodule::home.contact_us')
                                            </a>

                                        @auth()
                                            <li class="dropdown">
                                                <a class="dropdown-item dropdown-toggle {{route_exists('account')}}" href="{{route('front.account')}}">

                                                    @lang('frontmodule::home.my_account')
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown"><a class="dropdown-item" href="{{route('front.logout')}}">@lang('frontmodule::home.logout')</a>


                                                </ul>
                                            </li>
                                        @endauth
                                        @guest()

                                            <li class="dropdown">
                                                <a class="dropdown-item dropdown-toggle {{route_exists('login')}}" href="{{route('front.login')}}">

                                                    @lang('frontmodule::home.Login_register')
                                                </a>

                                            </li>


                                        @endguest


                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                    data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
