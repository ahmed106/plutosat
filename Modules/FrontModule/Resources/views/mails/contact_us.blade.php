@component('mail::message')
    # Introduction


    Mr: {{$data['name']}} sent you message
    Email :{{$data['email']}}
    Message : {{$data['message']}}


    Thanks,
    {{ config('app.name') }}
@endcomponent
