@extends('frontmodule::layouts.master')
@push('css')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

@endpush
@section('content')

    <div role="main" class="main">


        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="font-weight-bold text-dark">@lang('frontmodule::home.free_servers')</h1>

                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{url('/')}}">@lang('frontmodule::home.home')</a></li>
                            <li><a href="{{ route('front.account')}}">@lang('frontmodule::home.profile')</a></li>
                            <li class="active">@lang('frontmodule::home.free_servers')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <div class="container">

            <div class="row col-md-12">


                <div class="server col-md-12 {{$user_server != null ? 'd-none':''}}">


                    @if($server != null)


                        <h4>@lang('frontmodule::home.servers')</h4>

                        <table id="serversTable" class="table text-center table-bordered table-hover">
                            <thead>


                            <tr>
                                <td>@lang('frontmodule::home.number')</td>
                                <td>@lang('frontmodule::home.host')</td>
                                <td>@lang('frontmodule::home.port')</td>
                                <td>@lang('frontmodule::home.username')</td>
                                <td>@lang('frontmodule::home.password')</td>

                            </tr>


                            </thead>


                            <tbody>
                            <tr>

                                <td>{{$server->id}}</td>

                                <td>{{$server->host}}</td>
                                <td>{{$server->port}}</td>
                                <td>{{$server->username}}</td>

                                <td>
                                    <input disabled class="show_password form-control" type="password" value="{{$server->password}}">
                                    <form class="form-horizontal" action="{{route('front.password_show')}}" id="form-show-password" method="post">

                                        <input type="hidden" value="{{$server->id}}" name="server_id">
                                        <button type="submit" class="btn btn-success" id="show"><i class=" fa fa-eye"></i></button>
                                    </form>
                                </td>


                            </tr>
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-danger text-center col-md-12">@lang('frontmodule::home.no_data_yet')!</div>

                    @endif
                    {{--End If--}}

                </div>

                <div class="user_server col-md-12">
                    @if($user_server != null)


                        <h4>@lang('frontmodule::home.servers')</h4>

                        <table class="table text-center table-bordered table-hover">
                            <thead>


                            <tr>
                                <td>@lang('frontmodule::home.number')</td>
                                <td>@lang('frontmodule::home.host')</td>
                                <td>@lang('frontmodule::home.port')</td>
                                <td>@lang('frontmodule::home.username')</td>
                                <td>@lang('frontmodule::home.password')</td>

                            </tr>


                            </thead>


                            <tbody>
                            <tr>

                                <td>{{$user_server->id}}</td>

                                <td>{{$user_server->host}}</td>
                                <td>{{$user_server->port}}</td>
                                <td>{{$user_server->username}}</td>

                                <td>
                                    <input class="show_password form-control" type="password" value="{{$user_server->password}}">

                                    <a id="hidePass" hidden title="hide password" class="btn btn-primary mt-2" href=""><i class="fa fa-eye-slash"></i></a>
                                    <a id="showPass" title="show password" class="btn btn-success mt-2" href=""><i class="fa fa-eye"></i></a>
                                </td>


                            </tr>
                            </tbody>
                        </table>


                    @endif
                    {{--End If--}}
                </div>


            </div>

            <div class="row">

                <div class="col-md-3">
                    <style>
                        .pricingTable_37 {
                            margin-top: 10px;
                            margin-bottom: 10px !important;
                        }

                        .pricingTable_37 {
                            text-align: center;
                        }

                        .pricingTable_37 .pricingTable-header_37 {
                            background: #962744;
                        }

                        h3 {
                            color: #ffffff;
                        }

                        .pricingTable_37:hover .pricingTable-header_37 {
                            background-color: #ff4266;
                        }

                        .pricingTable_37 .heading_37 {
                            display: block;
                            padding-top: 25px;
                        }

                        .active .heading_37 > h3 {
                            color: #ffffff;
                        }

                        .active .pricingTable-header_37 {
                            background: #b21a1a;
                        }

                        .active:hover .pricingTable-header_37 {
                            background: #720202;
                        }

                        .active:hover .pricingTable-header_37 .heading_37 {
                            background-color: #720202 !important;
                        }

                        .active .btn-block_37 {
                            background: #b21a1a !important;
                        }

                        .active .btn-block_37 {
                            color: #ffffff !important;
                        }

                        .active .btn-block_37:hover {
                            background-color: #720202 !important;
                        }

                        .pricingTable_37 .heading_37:after {
                            content: "";
                            border-top: 1px solid rgba(255, 255, 255, 0.4);
                            display: inline-block;
                            width: 85%;
                        }

                        .pricingTable_37 .heading_37 > h3 {
                            margin: 0;
                            font-size: 20px;
                        }

                        .pricingTable_37 .heading_37 > span {
                            font-size: 13px;
                            margin-top: 5px;
                            display: block;
                        }

                        .pricingTable_37 .price-value_37 {
                            padding-bottom: 25px;
                            display: block;
                            font-size: 34px;
                            color: #fff;
                        }

                        .pricingTable-header_37 > .price-value_37 > .month_37 {
                            font-size: 14px;
                            display: inline-block;
                            margin: 5px;

                        }

                        .pricingTable_37 .price-value_37 > span {
                            display: block;
                            font-size: 14px;
                            line-height: 20px;
                        }

                        .pricingTable_37 .pricingContent_37 {
                            background: #151515;
                            color: #fefeff;
                        }

                        .pricingTable_37 .pricingContent_37 > i {
                            font-size: 60px;
                            margin-top: 20px;
                        }

                        .pricingTable_37 .pricingContent_37 ul {
                            list-style: none;
                            padding: 0;
                            margin-bottom: 0;
                            text-align: left;
                        }

                        .pricingTable_37 .pricingContent_37 ul li {
                            padding: 12px 0;
                            border-bottom: 1px solid #000;
                            border-top: 1px solid #333;
                            width: 85%;
                            margin: 0 auto;
                            color: #fff;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:first-child {
                            border-top: 0px none;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:last-child {
                            border-bottom: 0px none;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:before {

                            font-family: 'FontAwesome';
                            margin-right: 10px;
                            color: #962744;
                            transition: all 0.5s ease 0s;
                        }

                        .active .pricingContent_37 ul li:before {
                            color: #b21a1a;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:hover:before {
                            margin-right: 20px;
                        }

                        .pricingTable_37 .pricingTable-sign-up_37 {
                            padding: 20px 0;
                            background: #151515;
                            color: #fff;
                        }

                        .pricingTable_37 .pricingTable-sign-up_37 > span {
                            margin-top: 10px;
                            display: block;
                        }

                        .pricingTable_37 .btn-block_37 {
                            width: 50%;
                            margin: 0 auto;
                            border: 0px none;
                            background: #962744;
                            color: #ffffff;
                            padding: 10px;
                            border-radius: 3px;
                            font-size: 13px;
                            transition: all 0.5s ease 0s;
                            text-decoration: none !important;
                        }

                        .pricingTable_37 .btn-block_37:hover {
                            border-radius: 12px;
                            background-color: #ff4266;
                            color: #ffffff;
                        }

                        .pricingTable_37 .btn-block_37:before {
                            content: "\f07a";
                            font-family: 'FontAwesome';
                            margin-right: 10px;
                        }

                        @media screen and (max-width: 990px) {
                            .pricingTable_37 {
                                margin-bottom: 20px;
                            }
                        }
                    </style>
                    <div class="pricingTable_37 ">
                        <div class="pricingTable-header_37">
<span class="heading_37">
<h3>6 Months</h3>
</span>
                            <span class="price-value_37"> €5<span class="month_37"></span></span>
                        </div>
                        <div class="pricingContent_37">
                            <i class="empty"></i>
                            <ul>
                                <li style="text-align: center">
                                    1 GB/S INTERNET SPEED
                                </li>
                                <li style="text-align: center">
                                    FULL PACKAGES
                                </li>
                                <li style="text-align: center">
                                    2X IP ADDRESS (6LINES)
                                </li>
                                <li style="text-align: center">
                                    2X RECEIVERS
                                </li>
                                <li style="text-align: center">
                                    REAL LOCALS
                                </li>
                                <li style="text-align: center">
                                    FREEZE FREE
                                </li>
                            </ul>
                        </div>
                        <div class="pricingTable-sign-up_37">
                            <a href="https://sowl.co/xeAYN" class="btn btn-block_37 btn-default">Subscribe</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <style>
                        .pricingTable_37 {
                            margin-top: 10px;
                            margin-bottom: 10px !important;
                        }

                        .pricingTable_37 {
                            text-align: center;
                        }

                        .pricingTable_37 .pricingTable-header_37 {
                            background: #962744;
                        }

                        h3 {
                            color: #ffffff;
                        }

                        .pricingTable_37:hover .pricingTable-header_37 {
                            background-color: #ff4266;
                        }

                        .pricingTable_37 .heading_37 {
                            display: block;
                            padding-top: 25px;
                        }

                        .active .heading_37 > h3 {
                            color: #ffffff;
                        }

                        .active .pricingTable-header_37 {
                            background: #b21a1a;
                        }

                        .active:hover .pricingTable-header_37 {
                            background: #720202;
                        }

                        .active:hover .pricingTable-header_37 .heading_37 {
                            background-color: #720202 !important;
                        }

                        .active .btn-block_37 {
                            background: #b21a1a !important;
                        }

                        .active .btn-block_37 {
                            color: #ffffff !important;
                        }

                        .active .btn-block_37:hover {
                            background-color: #720202 !important;
                        }

                        .pricingTable_37 .heading_37:after {
                            content: "";
                            border-top: 1px solid rgba(255, 255, 255, 0.4);
                            display: inline-block;
                            width: 85%;
                        }

                        .pricingTable_37 .heading_37 > h3 {
                            margin: 0;
                            font-size: 20px;
                        }

                        .pricingTable_37 .heading_37 > span {
                            font-size: 13px;
                            margin-top: 5px;
                            display: block;
                        }

                        .pricingTable_37 .price-value_37 {
                            padding-bottom: 25px;
                            display: block;
                            font-size: 34px;
                            color: #fff;
                        }

                        .pricingTable-header_37 > .price-value_37 > .month_37 {
                            font-size: 14px;
                            display: inline-block;
                            margin: 5px;

                        }

                        .pricingTable_37 .price-value_37 > span {
                            display: block;
                            font-size: 14px;
                            line-height: 20px;
                        }

                        .pricingTable_37 .pricingContent_37 {
                            background: #151515;
                            color: #fefeff;
                        }

                        .pricingTable_37 .pricingContent_37 > i {
                            font-size: 60px;
                            margin-top: 20px;
                        }

                        .pricingTable_37 .pricingContent_37 ul {
                            list-style: none;
                            padding: 0;
                            margin-bottom: 0;
                            text-align: left;
                        }

                        .pricingTable_37 .pricingContent_37 ul li {
                            padding: 12px 0;
                            border-bottom: 1px solid #000;
                            border-top: 1px solid #333;
                            width: 85%;
                            margin: 0 auto;
                            color: #fff;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:first-child {
                            border-top: 0px none;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:last-child {
                            border-bottom: 0px none;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:before {

                            font-family: 'FontAwesome';
                            margin-right: 10px;
                            color: #962744;
                            transition: all 0.5s ease 0s;
                        }

                        .active .pricingContent_37 ul li:before {
                            color: #b21a1a;
                        }

                        .pricingTable_37 .pricingContent_37 ul li:hover:before {
                            margin-right: 20px;
                        }

                        .pricingTable_37 .pricingTable-sign-up_37 {
                            padding: 20px 0;
                            background: #151515;
                            color: #fff;
                        }

                        .pricingTable_37 .pricingTable-sign-up_37 > span {
                            margin-top: 10px;
                            display: block;
                        }

                        .pricingTable_37 .btn-block_37 {
                            width: 50%;
                            margin: 0 auto;
                            border: 0px none;
                            background: #962744;
                            color: #ffffff;
                            padding: 10px;
                            border-radius: 3px;
                            font-size: 13px;
                            transition: all 0.5s ease 0s;
                            text-decoration: none !important;
                        }

                        .pricingTable_37 .btn-block_37:hover {
                            border-radius: 12px;
                            background-color: #ff4266;
                            color: #ffffff;
                        }

                        .pricingTable_37 .btn-block_37:before {
                            content: "\f07a";
                            font-family: 'FontAwesome';
                            margin-right: 10px;
                        }

                        @media screen and (max-width: 990px) {
                            .pricingTable_37 {
                                margin-bottom: 20px;
                            }
                        }
                    </style>
                    <div class="pricingTable_37 ">
                        <div class="pricingTable-header_37">
<span class="heading_37">
<h3>12 Months</h3>
</span>
                            <span class="price-value_37"> €9<span class="month_37"></span></span>
                        </div>
                        <div class="pricingContent_37">
                            <i class="empty"></i>
                            <ul>
                                <li style="text-align: center">
                                    1 GB/S INTERNET SPEED
                                </li>
                                <li style="text-align: center">
                                    FULL PACKAGES
                                </li>
                                <li style="text-align: center">
                                    2X IP ADDRESS (6LINES)
                                </li>
                                <li style="text-align: center">
                                    2X RECEIVERS
                                </li>
                                <li style="text-align: center">
                                    REAL LOCALS
                                </li>
                                <li style="text-align: center">
                                    FREEZE FREE
                                </li>
                            </ul>
                        </div>
                        <div class="pricingTable-sign-up_37">
                            <a href="https://sowl.co/u9x2H" class="btn btn-block_37 btn-default">Subscribe</a>
                        </div>
                    </div>
                </div>

            </div>


        </div>


    @push('js')
        <!-- DataTables -->
            <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>


            <script>
                $(document).ready(function () {
                    $('#userTable').DataTable({
                        'paging': true,
                        'lengthChange': true,
                        'searching': true,
                        'ordering': true,
                        'info': true,
                        'autoWidth': false,

                    });
                })

            </script>

            <script>

                $(document).on('submit', '#form-show-password', function (e) {
                    e.preventDefault();
                    $('.show_password').attr('type', 'text');
                    route = $(this).attr('action');
                    data = $(this).serialize();
                    $.ajax({
                        'type': 'post',
                        'url': route,
                        data: {
                            '_token': '{{csrf_token()}}',
                            'data': data,
                            'user_id': '{{auth('web')->user()->id}}'
                        },
                        beforeSend: function () {

                        },
                        'statusCode': {

                            200: function (response) {
                                $('#show').hide();
                                setTimeout(function () {
                                    $('.show_password').attr('type', 'password');
                                }, 5000)
                            },
                            404: function (xhr) {
                                $.each(xhr.responseJSON.errors, function (key, value) {
                                    $('#validation-errors').append('<div class="alert alert-danger">' + value + '</div');
                                });
                            }
                        }


                    });


                })
            </script>

            <script>
                $(document).on('click', '#showPass', function (e) {
                    e.preventDefault();
                    td = $(this).parent();
                    $(this).attr('hidden', 'hidden');
                    td.find('#hidePass').removeAttr('hidden');
                    var _input = td.find('.show_password');
                    _input.attr('type', 'text')

                })

                $(document).on('click', '#hidePass', function (e) {
                    e.preventDefault();
                    td = $(this).parent();
                    $(this).attr('hidden', 'hidden');
                    td.find('#showPass').removeAttr('hidden');
                    var _input = td.find('.show_password');
                    _input.attr('type', 'password')

                })
            </script>
    @endpush


@endsection

