@extends('frontmodule::layouts.master')
@push('css')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

@endpush
@section('content')
    <div role="main" class="main">

        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="font-weight-bold text-dark">@lang('frontmodule::home.my_account')</h1>

                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{url('/')}}">@lang('frontmodule::home.home')</a></li>
                            <li class="active">@lang('frontmodule::home.my_account')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="container pt-3 pb-2">

            <div class="row pt-2">
                <div class="col-lg-3 mt-4 mt-lg-0">

                    <div class="d-flex justify-content-center mb-4">
                        <div class="profile-image-outer-container">
                            <div class="profile-image-inner-container bg-color-primary">
                                <img src="{{asset('images/default.jpg')}}">

                            </div>
                        </div>
                    </div>

                    <aside class="sidebar mt-2" id="sidebar">
                        <ul class="nav nav-list flex-column mb-5">
                            <li class="nav-item"><a class=" text-dark  user_info" href="#user_info">{{auth()->user()->name}}</a></li>

                            <li class="nav-item"><a class="servers" href="#servers">@lang('frontmodule::home.servers')</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-9">


                    <div id="servers" hidden="hidden">

                        @if(auth()->user()->type =='reseller')
                            @if($servers->count()>0)
                                <h4>@lang('frontmodule::home.my_servers')</h4>

                                <table id="serversTable" class="table text-center table-bordered table-hover">
                                    <thead>
                                    @if(auth()->user()->type =='reseller')
                                        <div class="float-right mb-2">
                                            <a href="{{route('front.assign')}}" class="  btn btn-danger"><i class="fa fa-server">@lang('frontmodule::home.assign_server_to_user')</i></a>
                                        </div>
                                    @endif


                                    <tr>
                                        <td>@lang('frontmodule::home.number')</td>
                                        <td>@lang('frontmodule::home.host')</td>
                                        <td>@lang('frontmodule::home.port')</td>
                                        <td>@lang('frontmodule::home.username')</td>
                                        <td>
                                            @lang('frontmodule::home.password')

                                        </td>
                                        @if(auth()->user()->type =='reseller')

                                            <td>@lang('frontmodule::home.status')</td>
                                            <td>@lang('frontmodule::home.control')</td>

                                        @endif


                                    </tr>


                                    </thead>

                                    @foreach($servers as $server)
                                        <tbody>
                                        <tr>
                                            <td>{{$server->server->id}}</td>
                                            <td>{{$server->server->host}}</td>
                                            <td>{{$server->server->port}}</td>
                                            <td>{{$server->server->username}}</td>
                                            <td>
                                                <input id="password_input" type="password" value="{{$server->server->password}}">
                                                <a class="btn btn-primary mt-2 hide_password d-none"><i class="fa fa-eye-slash"></i></a>
                                                <a class="btn btn-primary mt-2 show_password "><i class="fa fa-eye"></i></a>
                                            </td>

                                            @if(auth()->user()->type =='reseller')
                                                <td>
                                                    @if($server->is_booked == 1)

                                                        <a class="btn btn-danger">@lang('frontmodule::home.booked')</a>
                                                    @else
                                                        <a class="btn btn-primary">@lang('frontmodule::home.not_booked')</a>
                                                    @endif

                                                </td>


                                                <td>
                                                    <a href="{{route('front.assignUser',$server->server->id)}}" class="btn btn-success {{$server->is_booked ==1 ?'disabled':''}} ">Assign to User</a>

                                                </td>

                                            @endif

                                        </tr>
                                        </tbody>
                                    @endforeach</table>

                            @else
                                <div class="alert alert-danger text-center">@lang('frontmodule::home.dont_have_server_yet')</div>

                            @endif


                        @else
                            @if($free_server != null)


                                <h4>@lang('frontmdodule::home.servers')</h4>

                                <table id="serversTable" class="table text-center table-bordered table-hover">
                                    <thead>


                                    <tr>
                                        <td>@lang('frontmodule::home.number')</td>
                                        <td>@lang('frontmodule::home.host')</td>
                                        <td>@lang('frontmodule::home.port')</td>
                                        <td>@lang('frontmodule::home.username')</td>
                                        <td>@lang('frontmodule::home.password')</td>

                                    </tr>


                                    </thead>


                                    <tbody>
                                    <tr>

                                        <td>{{$free_server->id}}</td>

                                        <td>{{$free_server->host}}</td>
                                        <td>{{$free_server->port}}</td>
                                        <td>{{$free_server->username}}</td>

                                        <td>
                                            <input disabled class="show_password form-control" type="text" value="{{$free_server->password}}">
                                            <form class="form-horizontal" action="{{route('front.password_show')}}" id="form-show-password" method="post">

                                                <input type="hidden" value="{{$free_server->id}}" name="server_id">
                                            </form>
                                        </td>


                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-danger text-center">@lang('frontmodule::home.dont_have_server_yet')</div>

                            @endif
                            {{--End If--}}
                        @endif
                        {{--End If--}}


                    </div>

                    <div id="user_info">
                        <h4>@lang('frontmodule::home.my_account')</h4>
                        <form role="form" class="needs-validation">
                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label line-height-9 pt-2 text-2 required">@lang('frontmodule::home.name')</label>
                                <div class="col-lg-9">
                                    <input class="form-control text-3 h-auto py-2" type="text" name="name" value="{{auth()->user()->name}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 col-form-label form-control-label line-height-9 pt-2 text-2 required">@lang('frontmodule::home.email')</label>
                                <div class="col-lg-9">
                                    <input class="form-control text-3 h-auto py-2" type="email" name="email" value="{{auth()->user()->email}}" required>
                                </div>
                            </div>


                            {{--                            <div class="form-group row">--}}
                            {{--                                <div class="form-group col-lg-9">--}}

                            {{--                                </div>--}}
                            {{--                                <div class="form-group col-lg-3">--}}
                            {{--                                    <input type="submit" value="Save" class="btn btn-primary btn-modern float-right" data-loading-text="Loading...">--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </form>

                    </div>


                </div>
            </div>

        </div>

    </div>
    @push('js')
        <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>


        <script>

            $(document).ready(function () {
                $('#serversTable').DataTable({
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false,

                });
                $('.user_info').on('click', function (e) {


                    e.preventDefault();
                    $('.servers').removeClass('active');
                    $('#user_info').removeAttr('hidden');
                    $('#servers').attr('hidden', 'hidden');
                });

                $('.servers').on('click', function (e) {
                    e.preventDefault();
                    $(this).addClass('active');
                    $('.user_info').removeClass('active');

                    $('#user_info').attr('hidden', 'hidden');
                    $('#servers').removeAttr('hidden');
                })

            })
        </script>

        <script>

            $(document).on('submit', '#form-show-password', function (e) {
                e.preventDefault();
                $('.show_password').attr('type', 'text');
                route = $(this).attr('action');
                data = $(this).serialize();
                $.ajax({
                    'type': 'post',
                    'url': route,
                    data: {
                        '_token': '{{csrf_token()}}',
                        'data': data
                    },
                    beforeSend: function () {

                    },
                    'statusCode': {

                        200: function (response) {
                            setTimeout(function () {
                                $('.show_password').attr('type', 'password');
                            }, 5000)
                        },
                        404: function (xhr) {
                            $.each(xhr.responseJSON.errors, function (key, value) {
                                $('#validation-errors').append('<div class="alert alert-danger">' + value + '</div');
                            });
                        }
                    }


                });


            })
        </script>

        <script type="text/javascript">

            $('.show_password').on('click', function () {
                tr = $(this).closest('tr');
                tr.find('.show_password').addClass('d-none');
                tr.find('.hide_password').removeClass('d-none');
                tr.find('#password_input').attr('type', 'text')
                $(this).html('<i class="fa fa-eye"></i>')
            })
            $('.hide_password').on('click', function () {
                tr = $(this).closest('tr');
                tr.find('.hide_password').addClass('d-none');
                tr.find('.show_password').removeClass('d-none')
                tr.find('#password_input').attr('type', 'password')
                $(this).html('<i class="fa fa-eye-slash"></i>')
            })
        </script>

    @endpush
@endsection

