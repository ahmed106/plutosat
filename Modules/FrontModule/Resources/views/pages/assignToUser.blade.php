@extends('frontmodule::layouts.master')
@push('css')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

@endpush
@section('content')

    <div role="main" class="main">


        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="font-weight-bold text-dark">@lang('frontmodule::home.assign_servers_to_user')</h1>

                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{url('/')}}">@lang('frontmodule::home.home')</a></li>
                            <li><a href="{{ route('front.account')}}">@lang('frontmodule::home.profile')</a></li>
                            <li class="active">@lang('frontmodule::home.assign_servers_to_users')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <div class="container">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        {{$error}}
                    @endforeach
                    {{--End Foreach--}}

                </div>



            @endif
            {{--End If--}}

            <div class="row">


                <form class="col-md-12" action="{{route('front.assignedUser')}}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="server_id" value="{{$server_id}}">
                    <div class="form-group">
                        <label for="exampleInputEmail1">@lang('frontmodule::home.name')</label>
                        <input type="text" name="name" class="form-control" required id="exampleInputEmail1" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">@lang('frontmodule::home.email')</label>
                        <input type="email" name="email" required class="form-control" id="exampleInputPassword1" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">@lang('frontmodule::home.expiry_date')</label>
                        <input type="date" required name="expiry_date" class="form-control" id="exampleInputPassword1" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label for="notes">@lang('frontmodule::home.notes')</label>
                        <textarea name="note" id="notes" class="form-control" cols="50" rows="10"></textarea>
                    </div>


                    <button type="submit" class="btn btn-default col-md-12 btn btn-success mb-3"><i class=" fa fa-paper-plane"> @lang('frontmodule::home.assign')</i></button>
                </form>
                <br>
            </div>


        </div>


    @push('js')
        <!-- DataTables -->
            <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>


            <script>
                $(document).ready(function () {
                    $('#userTable').DataTable({
                        'paging': true,
                        'lengthChange': true,
                        'searching': true,
                        'ordering': true,
                        'info': true,
                        'autoWidth': false,

                    });
                })

            </script>
    @endpush

@endsection

