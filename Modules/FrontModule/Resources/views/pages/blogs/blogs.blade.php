@extends('frontmodule::layouts.master')

@section('content')
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div role="main" class="main">

        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="text-dark font-weight-bold text-8">{{$blog->title}}</h1>
                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{route('front.home')}}">@lang('frontmodule::home.home')</a></li>
                            <li class="active">@lang('frontmodule::home.blogs')</li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="container py-4">

            <div class="row">
                <div class="col-lg-3">
                    <aside class="sidebar">


                        <div class="tabs tabs-dark mb-4 pb-2">
                            <ul class="nav nav-tabs">
                                <li class="nav-item active"><a class="nav-link show active text-1 font-weight-bold text-uppercase" href="#popularPosts" data-toggle="tab">@lang('frontmodule::home.popular')</a></li>
                                <li class="nav-item"><a class="nav-link text-1 font-weight-bold text-uppercase" href="#recentPosts" data-toggle="tab">@lang('frontmodule::home.recent')</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="popularPosts">
                                    <ul class="simple-post-list">
                                        @foreach($resent_blogs as $r_blog)
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                        <a href="{{route('front.blogs_show',$r_blog->id)}}">
                                                            <img src="{{asset('images/blog/'.$r_blog->photo)}}" width="50" height="50" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="{{route('front.blogs_show',$r_blog->id)}}">{{$r_blog->title}}</a>
                                                    <div class="post-meta">
                                                        {{$r_blog->created_at->format('M  d Y')}}
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                        {{--End Foreach--}}


                                    </ul>
                                </div>
                                <div class="tab-pane" id="recentPosts">
                                    <ul class="simple-post-list">
                                        @foreach($last_blogs as $l_blog)
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                                        <a href="{{route('front.blogs_show',$l_blog->id)}}">
                                                            <img src="{{asset('images/blog/'.$l_blog->photo)}}" width="50" height="50" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="{{route('front.blogs_show',$l_blog->id)}}">{{$l_blog->title}}</a>
                                                    <div class="post-meta">
                                                        {{$l_blog->created_at->format('M d Y')}}
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                        {{--End Foreach--}}


                                    </ul>
                                </div>
                            </div>
                        </div>
                        <h5 class="font-weight-semi-bold pt-4">@lang('frontmodule::home.about')</h5>
                        {!! $about->content !!}
                        {{--                        <h5 class="font-weight-semi-bold pt-4">Find us on Facebook</h5>--}}
                        {{--                        <div class="fb-page" data-href="https://www.facebook.com/OklerThemes/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">--}}
                        {{--                            <blockquote cite="https://www.facebook.com/OklerThemes/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/OklerThemes/">Okler Themes</a></blockquote>--}}
                        {{--                        </div>--}}
                    </aside>
                </div>

                <div class="col-lg-9">
                    <div class="blog-posts single-post">

                        <article class="post post-large blog-single-post border-0 m-0 p-0">
                            <div class="post-image ml-0">

                                <a href="{{route('front.blogs_show',$blog->id)}}">
                                    <img style="height: 500px; width: 900px;" src="{{asset('images/blog/'.$blog->photo)}}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt=""/>
                                </a>
                            </div>

                            <div class="post-date ml-0">
                                <span class="day">{{$blog->created_at->format('d')}}</span>
                                <span class="month">{{$blog->created_at->format('M')}}</span>
                            </div>

                            <div class="post-content ml-0">

                                <h2 class="font-weight-semi-bold"><a href="{{route('front.blogs',$blog->id)}}">{{$blog->title}}</a></h2>

                                <div class="post-meta">
                                    <span><i class="far fa-user"></i> By <a href="#">Admin</a> </span>
                                    <span><i class="far fa-folder"></i>
                                        @foreach($blog->categories as $category)
                                            <a href="{{route('front.blogs',$category->id)}}">{{$category->title}}</a>,

                                        @endforeach
                                        {{--End Foreach--}}
                                    </span>
                                </div>

                                {!! $blog->description !!}
                                <div class="post-block mt-5 post-share">
                                    <h4 class="mb-3">Share this Post</h4>

                                    <!-- AddThis Button BEGIN -->
                                    <div class="addthis_toolbox addthis_default_style ">
                                        <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                        <a class="addthis_button_tweet"></a>
                                        <a class="addthis_button_pinterest_pinit"></a>
                                        <a class="addthis_counter addthis_pill_style"></a>
                                    </div>
                                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
                                    <!-- AddThis Button END -->

                                </div>


                            </div>
                        </article>

                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
