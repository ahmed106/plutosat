@extends('frontmodule::layouts.master')
@section('content')
    <div role="main" class="main">

        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="text-dark font-weight-bold text-8">{{__('frontmodule::home.blogs')}}</h1>
                        <span class="sub-title text-dark">@if(isset($category)) {{$category->title}} @endif</span>
                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{route('front.home')}}">{{__('frontmodule::home.home')}}</a></li>
                            <li class="active">{{__('frontmodule::home.blogs')}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="container py-4">

            <div class="row">
                <div class="col">
                    <div class="blog-posts">

                        <div class="row">
                            @if($category)
                                @foreach($category->blogs as $blog)
                                    <div class="col-md-4">
                                        <article class="post post-medium border-0 pb-0 mb-5">
                                            <div class="post-image">
                                                <a href="{{route('front.blogs_show',$blog->id)}}">
                                                    <img style="height: 150px; width: 350px;" src="{{asset('images/blog/'.$blog->photo)}}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt=""/>
                                                </a>
                                            </div>

                                            <div class="post-content">

                                                <h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2"><a href="{{route('front.blogs_show',$blog->id)}}">{{$blog->title}}</a></h2>
                                                <p>{{strip_tags(\Illuminate\Support\Str::limit($blog->description,200))}}</p>

                                                <div class="post-meta">
                                                    <span><i class="far fa-user"></i> By <a href="#">Admin</a> </span>


                                                    <span class="d-block mt-2"><a href="{{route('front.blogs_show',$blog->id)}}" class="btn btn-xs btn-light text-1 text-uppercase">Read More</a></span>
                                                </div>

                                            </div>
                                        </article>
                                    </div>
                                @endforeach
                                {{--End Foreach--}}
                            @else
                                <div class="container text-center">

                                    <div class="alert alert-danger">{{__('frontmodule::home.no_date_yet')}}</div>
                                </div>

                            @endif
                            {{--End If--}}


                        </div>

                        <div class="row">
                            <div class="col">

                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection
