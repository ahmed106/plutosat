@extends('frontmodule::layouts.master')

@section('content')
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div role="main" class="main">

        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="text-dark font-weight-bold text-8">{{$about->title}}</h1>
                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{route('front.home')}}">@lang('frontmodule::home.home')</a></li>
                            <li class="active">@lang('frontmodule::home.about')</li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="container py-4">

            <div class="row">


                <div class="col-lg-12">
                    <div class="blog-posts single-post">

                        <article class="post post-large blog-single-post border-0 m-0 p-0">
                            <div class="post-image ml-0">

                                <a href="{{route('front.about',$about->id)}}">
                                    <img style="height: 500px;" src="{{asset('images/pages/'.$about->photo)}}" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt=""/>
                                </a>
                            </div>


                            <div class="post-content ml-0">


                                {!! $about->content !!}


                            </div>
                        </article>

                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
