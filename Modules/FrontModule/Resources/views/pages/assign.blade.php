@extends('frontmodule::layouts.master')
@push('css')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

@endpush
@section('content')

    <div role="main" class="main">

        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="font-weight-bold text-dark">@lang('frontmodule::home.assign_servers_to_users')</h1>

                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{url('/')}}">@lang('frontmodule::home.home')</a></li>
                            <li><a href="{{ route('front.account')}}">@lang('frontmodule::home.profile')</a></li>
                            <li class="active">@lang('frontmodule::home.assign_servers_to_users')</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <div class="container">
            <spna>@lang('frontmodule::home.available_servers')</spna>
            [@foreach($servers as $server)

                {{$server->server_id}},
            @endforeach
            {{--End Foreach--}}]
            @if($users->count()>0)

                <table id="userTable" class="table text-center table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('frontmodule::home.username')</th>
                        <th>@lang('frontmodule::home.email')</th>
                        <th>@lang('frontmodule::home.assigning')</th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($users as $index => $user)


                        <tr>
                            <td> {{$index+1}} </td>


                            <td> {{$user->name}} </td>
                            <td> {{$user->email}} </td>
                            <form action="{{route('front.assignToUser')}}" method="post">
                                {!! csrf_field() !!}

                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <td>
                                    <label for="from" class="control-label">@lang('frontmodule::home.from')</label> <input type="number" name="from">
                                    <label for="to" class="control-label">@lang('frontmodule::home.to')</label> <input type="number" name="to">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-paper-plane"> @lang('frontmodule::home.assign')</i></button>

                                </td>


                            </form>
                        </tr>


                    @endforeach
                    </tbody>
                </table>

            @else
                <div class="alert alert-danger text-center">@lang('frontmodule::home.no_data_yet')</div>

            @endif
        </div>


    </div>



    @push('js')
        <!-- DataTables -->
        <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>


        <script>
            $(document).ready(function () {
                $('#userTable').DataTable({
                    'paging': true,
                    'lengthChange': true,
                    'searching': true,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false,

                });
            })

        </script>
    @endpush

@endsection

