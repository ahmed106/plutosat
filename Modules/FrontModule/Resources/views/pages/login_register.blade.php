@extends('frontmodule::layouts.master')

@section('content')

    <div role="main" class="main">

        <section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 align-self-center p-static order-2 text-center">


                        <h1 class="font-weight-bold text-dark">@lang('frontmodule::home.login')</h1>

                    </div>

                    <div class="col-md-12 align-self-center order-1">


                        <ul class="breadcrumb d-block text-center">
                            <li><a href="{{url('/')}}">@lang('frontmodule::home.home')</a></li>
                            <li class="active">@lang('frontmodule::home.pages')</li>
                        </ul>
                    </div>
                </div>

            </div>

        </section>


        @include('frontmodule::includes.messages')

        <br>
        <div class="container py-4">

            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-5 mb-5 mb-lg-0">
                    <h2 class="font-weight-bold text-5 mb-0">@lang('frontmodule::home.login')</h2>
                    <form action="{{route('front.doLogin')}}" id="" method="post" class="needs-validation">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="text-color-dark text-3">@lang('frontmodule::home.email') <span class="text-color-danger">*</span></label>
                                <input type="text" name="email" value="" class="form-control form-control-lg text-4" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="text-color-dark text-3">@lang('frontmodule::home.password') <span class="text-color-danger">*</span></label>
                                <input type="password" name="password" value="" class="form-control form-control-lg text-4" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <button type="submit" class="btn btn-dark btn-modern btn-block text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">@lang('frontmodule::home.login')</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-md-6 col-lg-5">
                    <h2 class="font-weight-bold text-5 mb-0">@lang('frontmodule::home.register')</h2>
                    <form action="{{route('front.register')}}" id="frmSignUp" method="post">
                        {{csrf_field()}}
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="text-color-dark text-3">@lang('frontmodule::home.email') <span class="text-color-danger">*</span></label>
                                <input type="email" name="email" value="" class="form-control form-control-lg text-4" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="text-color-dark text-3">@lang('frontmodule::home.username') <span class="text-color-danger">*</span></label>
                                <input type="text" name="name" value="" class="form-control form-control-lg text-4" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="text-color-dark text-3">@lang('frontmodule::home.password') <span class="text-color-danger">*</span></label>
                                <input type="password" name="password" value="" class="form-control form-control-lg text-4" required>
                            </div>
                        </div>

                        @if($countries->count() > 0)
                            <div class="form-row">
                                <div class="form-group col">
                                    <label class="text-color-dark text-3">@lang('frontmodule::home.country') ! <span class="text-color-danger">*</span></label>
                                    <select name="country_id" id="type" class="form-control">
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                        {{--End Foreach--}}


                                    </select>
                                </div>
                            </div>
                        @endif
                        {{--End If--}}

                        <div class="form-row">
                            <div class="form-group col">
                                <label class="text-color-dark text-3">@lang('frontmodule::home.you_are') ! <span class="text-color-danger">*</span></label>
                                <select name="type" id="type" class="form-control">
                                    <option value="user">@lang('frontmodule::home.user')</option>
                                    <option value="reseller">@lang('frontmodule::home.reseller')</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col">
                                <button type="submit" class="btn btn-dark btn-modern btn-block text-uppercase rounded-0 font-weight-bold text-3 py-3" data-loading-text="Loading...">@lang('frontmodule::home.register')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
@endsection
