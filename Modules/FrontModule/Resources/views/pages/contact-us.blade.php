@extends('frontmodule::layouts.master')
@section('content')

    <div role="main" class="main">

        <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
        <br><br><br>
        <div class="container">

            <div class="row py-4">
                <div class="col-lg-6">

                    <h2 class="font-weight-bold text-8 mt-2 mb-0">@lang('frontmodule::home.contact_us')</h2>


                    <form action="{{route('front.contact')}}" method="POST">
                        {{csrf_field()}}
                        @if(session('success'))
                            <div class=" alert alert-success  mt-4">


                                <strong>@lang('frontmodule::home.success')</strong> @lang('frontmodule::home.your_message_has_been_sent_to_us.')

                                {{--End If--}}
                            </div>
                        @endif

                        <div class="form-row">
                            <div class="form-group col-lg-6">
                                <label class="mb-1 text-2">@lang('frontmodule::home.full_name')</label>
                                <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control text-3 h-auto py-2" name="name" required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="mb-1 text-2">@lang('frontmodule::home.email_address')</label>
                                <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control text-3 h-auto py-2" name="email" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="mb-1 text-2">@lang('frontmodule::home.subject')</label>
                                <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control text-3 h-auto py-2" name="subject" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <label class="mb-1 text-2">@lang('frontmodule::home.message')</label>
                                <textarea maxlength="5000" data-msg-required="Please enter your message." rows="8" class="form-control text-3 h-auto py-2" name="message" required></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">
                                <input type="submit" value="@lang('frontmodule::home.send_message')" class="btn btn-primary btn-modern" data-loading-text="Loading...">
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-lg-6">

                    <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
                        <h4 class="mt-2 mb-1">@lang('frontmodule::home.our') <strong>@lang('frontmodule::home.office')</strong></h4>
                        <ul class="list list-icons list-icons-style-2 mt-2">
                            <li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">@lang('frontmodule::home.address'):</strong> {{$config['address']}}</li>
                            <li><i class="fas fa-phone top-6"></i> <strong class="text-dark">@lang('frontmodule::home.phone'):</strong> {{$config['phone']}}</li>
                            <li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">@lang('frontmodule::home.email'):</strong> <a href="mailto:{{$config['email']}}">{{$config['email']}}</a></li>
                        </ul>
                    </div>


                </div>

            </div>

        </div>

    </div>

@endsection
@push('js')
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
    <script>

        /*
        Map Settings

            Find the Latitude and Longitude of your address:
                - https://www.latlong.net/
                - http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

        */
        function initializeGoogleMaps() {
            // Map Markers
            var mapMarkers = [{
                address: "New York, NY 10017",
                html: "<strong>New York Office</strong><br>New York, NY 10017",
                icon: {
                    image: "img/pin.png",
                    iconsize: [26, 46],
                    iconanchor: [12, 46]
                },
                popup: true
            }];

            // Map Initial Location
            var initLatitude = 40.75198;
            var initLongitude = -73.96978;

            // Map Extended Settings
            var mapSettings = {
                controls: {
                    draggable: (($.browser.mobile) ? false : true),
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true
                },
                scrollwheel: false,
                markers: mapMarkers,
                latitude: initLatitude,
                longitude: initLongitude,
                zoom: 11
            };

            var map = $('#googlemaps').gMap(mapSettings);

            // Map text-center At
            var mapCenterAt = function (options, e) {
                e.preventDefault();
                $('#googlemaps').gMap("centerAt", options);
            }
        }

        // Initialize Google Maps when element enter on browser view
        theme.fn.intObs('.google-map', 'initializeGoogleMaps()', {});

    </script>
@endpush
