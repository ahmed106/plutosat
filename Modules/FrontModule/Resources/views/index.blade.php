@extends('frontmodule::layouts.master')
@push('css')

@endpush

@section('content')

    <div role="main" class="main">
        <div class="owl-carousel owl-carousel-light owl-carousel-light-init-fadeIn owl-theme manual dots-inside dots-horizontal-center show-dots-hover show-dots-xs dots-light nav-inside nav-inside-plus nav-dark nav-md nav-font-size-md show-nav-hover mb-0"
             data-plugin-options="{'autoplayTimeout': 3000}" style="height: 100vh;">
            <div class="owl-stage-outer">
                <div class="owl-stage">

                    <!-- Carousel Slide 1 -->
                    @foreach($sliders as $slider)
                        <div class="owl-item position-relative overlay overlay-show overlay-op-9 pt-5"
                             style="background-image: url({{asset('images/sliders/'.$slider->photo)}}); background-size: cover; background-position: center; height: 100vh;">
                            <div class="container position-relative z-index-3 h-100">
                                <div class="row justify-content-center align-items-center h-100">
                                    <div class="col-lg-7 text-center">
                                        <div class="d-flex flex-column align-items-center justify-content-center h-100">
                                            <h3 class="position-relative text-color-light text-5 line-height-5 font-weight-medium ls-0 px-4 mb-1 appear-animation"
                                                data-appear-animation="fadeInDownShorterPlus"
                                                data-plugin-options="{'minWindowWidth': 0}">


                                            </h3>
                                            <h1 class="text-color-light font-weight-extra-bold text-10 text-sm-12-13 line-height-1 line-height-sm-3 mb-2 appear-animation"
                                                data-appear-animation="blurIn" data-appear-animation-delay="500"
                                                data-plugin-options="{'minWindowWidth': 0}"> {{$slider->title}}</h1>
                                            <p class="text-4-5 text-color-light font-weight-light opacity-7 text-center mb-4"
                                               data-plugin-animated-letters
                                               data-plugin-options="{'startDelay':0, 'minWindowWidth': 0, 'animationSpeed': 0}">
                                                {{strip_tags($slider->description)}}</p>
                                            <div class="appear-animation" data-appear-animation="fadeInUpShorter"
                                                 data-appear-animation-delay="50000">
                                                {{--                                                <div class="d-flex align-items-center mt-2">--}}
                                                {{--                                                    <a href="#"--}}
                                                {{--                                                       class="btn btn-light btn-modern text-color-primary font-weight-bold text-2 py-3 btn-px-4">LEARN--}}
                                                {{--                                                        MORE</a>--}}
                                                {{--                                                    <a href="#"--}}
                                                {{--                                                       class="btn btn-primary btn-modern font-weight-bold text-2 py-3 btn-px-4 ml-4">GET--}}
                                                {{--                                                        STARTED NOW <i class="fas fa-arrow-right ml-2"></i></a>--}}
                                                {{--                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--End Foreach--}}


                </div>
            </div>
            <div class="owl-nav">
                <button type="button" role="presentation" class="owl-prev"></button>
                <button type="button" role="presentation" class="owl-next"></button>
            </div>
            <div class="owl-dots mb-5">
                <button role="button" class="owl-dot active"><span></span></button>
                <button role="button" class="owl-dot"><span></span></button>
            </div>
        </div>

        <section class="section bg-color-light border-0 m-0">
            <div class="container">
                <div class="row text-center text-md-left mt-4">
                    @foreach($achievements as $achieve)


                        <div class="col-md-4 mb-4 mb-md-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">

                            {{--End Foreach--}}
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-4">
                                    <img style="height: 100px;" class="img-fluid mb-4" src="{{asset('images/acheives/'.$achieve->icon)}}" alt="">
                                </div>
                                <div class="col-lg-8">
                                    <h2 class="font-weight-bold text-5 line-height-5 mb-1"></h2>
                                    <p class="mb-0">{{strip_tags(\Illuminate\Support\Str::limit( $achieve->content,100))}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>

        <section class="section section-height-3 bg-color-grey-scale-1 m-0 border-0">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0">
                        <a href="{{route('front.about')}}"><h2 class="text-color-dark font-weight-normal text-6 mb-2">@lang('frontmodule::home.who') <strong class="font-weight-extra-bold">@lang('frontmodule::home.we_are')</strong></h2></a>
                        @if(isset($page->content))

                            {!! \Illuminate\Support\Str::limit($page->content,150) !!}
                        @endif
                        {{--End If--}}
                    </div>

                    <div class="col-sm-8 col-md-6 col-lg-4 offset-sm-4 offset-md-4 offset-lg-2 mt-sm-5" style="top: 1.7rem;">
                        @if(isset($page->photo))

                            <a href="{{route('front.about')}}"> <img src="{{asset('images/pages/'.$page->photo)}}" style="height: 300px; top: -50px;" class="img-fluid position-relative appear-animation mb-2" data-appear-animation="expandIn" data-appear-animation-delay="600" alt=""/></a>
                        @endif
                        {{--End If--}}
                    </div>
                </div>
            </div>
        </section>


        <section class="section section-height-3 bg-primary border-0 m-0 appear-animation" data-appear-animation="fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
                        <h2 class="font-weight-bold text-color-light text-6 mb-4">@lang('frontmodule::home.latest_posts')</h2>
                    </div>
                </div>
                <div class="row recent-posts appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">

                    @foreach($blogs as $blog)
                        <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">

                            <article>
                                <div class="row">
                                    <div class="col">
                                        <a href="{{route('front.blogs_show',$blog->id)}}" class="text-decoration-none">
                                            <img style="height: 150px; width: 260px;" src="{{asset('images/blog/'.$blog->photo)}}" class="img-fluid hover-effect-2 mb-3" alt=""/>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-auto pr-0">
                                        <div class="date">
                                            <span class="day bg-color-light text-color-dark font-weight-extra-bold">{{$blog->created_at->format('d')}}</span>
                                            <span class="month bg-color-light font-weight-semibold text-color-primary text-1">{{$blog->created_at->format('M')}}</span>
                                        </div>
                                    </div>
                                    <div class="col pl-1">
                                        <h4 class="line-height-3 text-4"><a href="{{route('front.blogs_show',$blog->id)}}" class="text-light">{{$blog->title}}</a></h4>
                                        <p class="text-color-light line-height-5 opacity-6 pr-4 mb-1">{{strip_tags(\Illuminate\Support\Str::limit($blog->description,50))}}</p>
                                        <a href="{{route('front.blogs_show',$blog->id)}}" class="read-more text-color-light font-weight-semibold text-2">@lang('frontmodule::home.more') <i class="fas fa-chevron-right text-1 ml-1"></i></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    @endforeach
                    {{--End Foreach--}}


                </div>
            </div>
        </section>

        <section class="section bg-color-grey-scale-1 section-height-3 border-0 m-0">
            <div class="container pb-2">
                <div class="row">
                    <div class="col-lg-6 text-center text-md-left mb-5 mb-lg-0">
                        <h2 class="text-color-dark font-weight-normal text-6 mb-2">@lang('frontmodule::home.about') <strong class="font-weight-extra-bold">@lang('frontmodule::home.our_clients')</strong></h2>

                        <div class="row justify-content-center my-5">

                            @foreach($partners->take(3) as $partner)
                                <div class="col-8 text-center col-md-4">
                                    <a href="{{$partner->link}}"><img style="height: 100px; width: 100px;" src="{{asset('images/partners/'.$partner->photo)}}" class="img-fluid hover-effect-3" alt=""/></a>
                                </div>

                            @endforeach
                            {{--End Foreach--}}

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="owl-carousel owl-theme nav-style-1 stage-margin" data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 1}, '1200': {'items': 1}}, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 40}">
                            @foreach($testimonials as $testimonial)
                                <div>
                                    <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark testimonial-remove-right-quote pl-md-4 mb-0">
                                        <div class="testimonial-author">
                                            <img style="width: 60px; height: 60px;" src="{{asset('images/testimonials/'.$testimonial->photo)}}" class="img-fluid rounded-circle mb-0" alt="">
                                        </div>
                                        <blockquote>
                                            <p class="text-color-dark text-4 line-height-5 mb-0">{{strip_tags($testimonial->quote)}}</p>
                                        </blockquote>
                                        <div class="testimonial-author">
                                            <p><strong class="font-weight-extra-bold text-2">{{$testimonial->name}}</strong><span>{{$testimonial->job_title}}</span></p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            {{--End Foreach--}}


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('js')

@endpush
