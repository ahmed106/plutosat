<!DOCTYPE html>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{$config['title']}}</title>

    <meta name="keywords" content="HTML5 Template"/>
    <meta name="description" content="Porto - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    @include('frontmodule::includes.css')
    @stack('css')

</head>

<body data-plugin-page-transition>
<div class="body">
    @include('frontmodule::includes.header')
    @yield('content')
    @include('frontmodule::includes.footer')
</div>

@include('frontmodule::includes.js')
@stack('js')
</body>

</html>
