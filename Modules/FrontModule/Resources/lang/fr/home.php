<?php


return [
    /*French*/

    'lang' => 'Langue',
    'home' => 'Accueil',
    'about' => 'Sur ',
    'cccam' => 'CCCam',
    'contact_us' => 'Nous contacter',
    'blogs' => 'Blogs',
    'my_account' => 'Mon compte',
    'Login_register' => "Connexion | S'inscrire",
    'opening_hours' => "Horaires d'ouvertures",
    'social_media' => 'Des médias sociaux',
    'no_date_yet' => 'Pas encore de date',
    'who' => 'OMS',
    'we_are' => 'Nous sommes',
    'latest_posts' => 'Derniers messages',
    'more' => 'Suite',
    'our_clients' => 'Nos clients',
    'popular' => 'populaire',
    'recent' => 'Récente',
    'full_name' => 'Nom et prénom',
    'email_address' => 'Adresse e-mail',
    'subject' => 'Matière',
    'message' => 'Un message',
    'send_message' => 'Envoyer le message',
    'our' => 'Notre',
    'office' => 'Bureau',
    'address' => 'Adresse',
    'phone' => 'Téléphoner',
    'email' => 'E-mail',
    'login' => 'Connexion',
    'register' => "S'inscrire",
    'password' => 'Mot de passe',
    'username' => "Nom d'utilisateur",
    'country' => 'De campagne',
    'you_are' => 'Tu es',
    'user' => 'Utilisateur',
    'reseller' => 'Revendeur',
    'free_servers' => 'Serveurs gratuits',
    'no_data_yet' => 'Pas encore de données',
    'logout' => 'Se déconnecter',
    'servers' => 'Les serveurs',
    'name' => 'Nom',
    'pages' => 'des pages',
    'dont_have_server_yet' => "Je n'ai pas encore de serveurs",
    'profile' => 'Profil',
    'my_servers' => 'Mes serveurs',
    'assign_server_to_user' => "Attribuer le serveur à l'utilisateur",
    'number' => 'numéro',
    'host' => 'Hôte',
    'port' => 'Port',
    'status' => 'Statut',
    'control' => 'Contrôler',
    'booked' => 'Réservé',
    'not_booked' => 'Non réservé',
    'assign' => 'Attribuer',
    'expiry_date' => "Expiry Date",
    'assign_servers_to_user' => 'Attribuer des serveurs aux utilisateurs',
    'assign_servers_to_users' => 'Attribuer des serveurs aux utilisateurs',
    'available_servers' => 'Serveurs disponibles',
    'to' => 'À',
    'from' => 'De',
    'assigning' => 'Attribution',
    'technicians' => 'Technicians'

];
