<?php

namespace Modules\FrontModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AreaModule\Entities\Country;
use Modules\ResellersModule\Entities\Reseller;
use Modules\ResellersModule\Entities\ResellerServer;
use Modules\ServerModule\Entities\FreeServer;
use Modules\ServerModule\Entities\Server;
use Modules\UserModule\Entities\User;
use Modules\UserModule\Entities\UserServer;

class AuthController extends Controller
{
    public function login_form()
    {
        $countries = Country::with('translation')->get();

        return view('frontmodule::pages.login_register', compact('countries'));

    }//end function


    public function register(Request $request)
    {

        $request->validate([
            'email' => 'required|unique:users,email',
            'name' => 'required|min:4',
            'password' => 'required',
            'country_id' => 'sometimes|nullable'
        ]);
        $data = $request->except('_token');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->type = $data['type'];
        $user->country_id = $data['country_id'];
        $user->approval = 0;
        $user->save();
        $user->resellers()->create([
            'user_id' => $user->id,
        ]);
        if ($user->type == 'user') {
            if (auth()->guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {
                return redirect()->intended(route('front.account'));
            }

        }
        session()->flash('error', 'wait Admin Approval');
        return redirect('/');

    }//end function


    public function doLogin(Request $request)
    {
        if (auth()->guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {

            if (auth()->user()->type == 'user') {
                return redirect()->intended(route('front.account'));

            } else {
                if (auth()->user()->approval == 1) {
                    return redirect()->intended(route('front.account'));
                } else {
                    session()->flash('error', 'wait Admin Approval');
                    return redirect()->back();
                }
            }

        } else {
            return redirect()->back()->with('error', 'Email Or Password is incorrect');
        }

    }//end function

    public function account()
    {


        $free_server = '';
        if (auth()->user()->type == 'reseller') {

            $reseller = Reseller::where('user_id', auth()->user()->id)->first();
            $servers = ResellerServer::where('reseller_id', $reseller->id)->get();


        } else {
            $user = User::where('id', auth()->user()->id)->first();
            $free_server = FreeServer::whereHas('userServer', function ($q) use ($user) {
                $q->where('user_id', $user->id);

            })->orderByDesc('id')->first();


            $servers = UserServer::where('user_id', $user->id)->where('expired', 0)->get();
        }

        return view('frontmodule::pages.account', compact('servers', 'free_server'));

    }//end function


    public function logout()
    {
        auth()->guard('web')->logout();
        return redirect(route('front.login'));

    }//end function

    public function assign()
    {
        $users = User::where('type', 'user')->get();
        $reseller = Reseller::where('user_id', auth()->user()->id)->first();
        $servers = ResellerServer::where('is_booked', 0)->where('reseller_id', $reseller->id)->get();

        return view('frontmodule::pages.assign', compact('users', 'servers'));
    }//end function

    public function assignServersToUsers(Request $request)
    {


        $user_id = $request->user_id;
        $reseller_id = Reseller::where('user_id', auth()->user()->id)->first()->id;


        $server_from = ResellerServer::where('server_id', $request->from)->first();

        $server_to = ResellerServer::where('server_id', $request->to)->first();

        $servers = ResellerServer::where('is_booked', 0)->count();


        if ($servers > 0 && $server_from != null && $server_to != null) {
            for ($server_from->server_id; $server_from->server_id <= $server_to->server_id; $server_from->server_id++) {

                $userServer = UserServer::where('server_id', $server_from->server_id)->first();

                if ($userServer) {

                    return redirect()->back()->with('error', 'Server Number ' . $userServer->server_id . ' Is Already Booked');
                }


                UserServer::create(
                    [
                        'user_id' => $user_id,
                        'server_id' => $server_from->server_id,
                        'reseller_id' => $reseller_id,
                    ]
                );
                ResellerServer::where('server_id', $server_from->server_id)->update(['is_booked' => 1]);

            }
            return redirect()->back()->with('success', 'data stored');
        } else {
            return redirect()->back()->with('error', 'Not Available Server');
        }

    }//end function


    public function assignToUser($server_id)
    {
        return view('frontmodule::pages.assignToUser', compact('server_id'));

    }//end function

    public function assignedToUser(Request $request)
    {


        $reseller_id = Reseller::where('user_id', auth()->user()->id)->first()->id;

        $reseller_server = ResellerServer::where('server_id', $request->server_id)->first();
        $reseller_server->update(['is_booked' => 1]);

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:user_servers,email',
            'server_id' => 'required|unique:user_servers,server_id'
        ], [
            'server_id.unique' => 'Server has been already Booked'
        ]);
        $user_server = UserServer::create([
            'server_id' => $request->server_id,
            'reseller_id' => $reseller_id,
            'name' => $request->name,
            'email' => $request->email,
            'expiry_date' => $request->expiry_date,
        ]);
        Server::where('id', $request->server_id)->update(['notes' => $request->notes]);
        return redirect()->route('front.account');

    }//end function


}
