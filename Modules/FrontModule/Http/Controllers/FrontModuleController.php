<?php

namespace Modules\FrontModule\Http\Controllers;

use App\Events\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\BlogModule\Entities\Blog;
use Modules\BlogModule\Repository\BlogCategoryRepository;
use Modules\BlogModule\Repository\BlogRepository;
use Modules\ServerModule\Entities\FreeServer;
use Modules\UserModule\Entities\User;
use Modules\UserModule\Entities\UserFreeServer;
use Modules\WidgetsModule\Repository\AchieveRepository;
use Modules\WidgetsModule\Repository\PageRepository;
use Modules\WidgetsModule\Repository\PartnerRepository;
use Modules\WidgetsModule\Repository\SliderRepository;
use Modules\WidgetsModule\Repository\TeamRepository;
use Modules\WidgetsModule\Repository\TestimonialRepository;

class FrontModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public $sliders;
    public $testimonials;
    public $partners;
    public $teams;
    public $blogs;
    public $pages;
    public $achieve;
    public $blog_category;

    public function __construct(
        SliderRepository $sliderRepository,
        TestimonialRepository $testimonials,
        PartnerRepository $partners,
        TeamRepository $teams,
        BlogRepository $blogs,
        PageRepository $pages,
        AchieveRepository $achieve,
        BlogCategoryRepository $blog_category
    )
    {
        $this->sliders = $sliderRepository;
        $this->testimonials = $testimonials;
        $this->partners = $partners;
        $this->teams = $teams;
        $this->blogs = $blogs;
        $this->pages = $pages;
        $this->achieve = $achieve;
        $this->blog_category = $blog_category;

    }//end function

    public function index()
    {


        $sliders = $this->sliders->findAll();
        $testimonials = $this->testimonials->findAll();

        $partners = $this->partners->findAll()->take(3);
        $teams = $this->teams->findAll();
        $blogs = $this->blogs->findAll()->take('3');
        $page = $this->pages->findByTitle('who we are');
        $achievements = $this->achieve->findAll();


        return view('frontmodule::index', compact('sliders', 'testimonials', 'partners', 'teams', 'blogs', 'page', 'achievements'));
    }

//    public function blog_categories()
//    {
//        $categories = $this->blog_category->findAllWithPaginate(20);
//
//        return view('frontmodule::pages.blogs.blog_categories', compact('categories'));
//
//    }//end function
//
//    public function blog_categories_show($id)
//    {
//        $category = $this->blog_category->find($id);
//
//        return view('frontmodule::pages.blogs.blog_categories_show', compact('category'));
//
//    }//end function


    public function blogs()
    {
        $category = $this->blog_category->findFirst();
        return view('frontmodule::pages.blogs.blog_categories', compact('category'));

    }//end function

    public function blogs_show($id)
    {

        $blog = $this->blogs->find($id);

        $resent_blogs = Blog::orderBy('id', 'asc')->get()->take(3);
        $last_blogs = Blog::orderBy('id', 'desc')->get()->take(3);
        $categories = $this->blog_category->findAllWithPaginate(20);
        $about = $this->pages->findByTitle('who we are')->first();


        return view('frontmodule::pages.blogs.blogs', compact('blog', 'categories', 'about', 'last_blogs', 'resent_blogs'));

    }//end function

    public function about()
    {
        $about = $this->pages->findByTitle('who we are');
        return view('frontmodule::pages.about', compact('about'));
    }//end function

    public function technicians()
    {
        $page = $this->pages->findByTitle('technicians');

        return view('frontmodule::pages.technicians', compact('page'));
    }//end function


    public function contact_us()
    {

        return view('frontmodule::pages.contact-us');

    }//end function

    public function contact(Request $request)
    {
        $data = $request->except('_token');

        event(new SendEmail($data));

        session()->flash('success', 'data sent successfully');
        return redirect()->back();

    }//end function

    public function passwordShow(Request $request)
    {
        parse_str($request->data, $data);
        $server = FreeServer::find($data['server_id']);
        $server->update(['is_shown' => 1]);
        $user = User::find($request->user_id);
        $user->freeServer()->create(['user_id' => $user->id, 'free_server_id' => $data['server_id']]);
        return \response()->json('success', 200);


    }//end function

    public function freeServer()
    {
        $server = FreeServer::where('is_shown', 0)->first();
        $user_server = auth()->user()->freeServer;

        if ($user_server) {
            $user_server = FreeServer::where('id', $user_server->free_server_id)->first();
        }


        return view('frontmodule::pages.free_servers', compact('server', 'user_server'));

    }//end function


}
