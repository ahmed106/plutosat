<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ], function () {

    Route::group(['middleware' => 'web', 'prefix' => '', 'name' => ''], function () {

        Route::get('/', 'FrontModuleController@index')->name('front.home');
        Route::get('/login|register', 'AuthController@login_form')->name('front.login');

        Route::post('/register', 'AuthController@register')->name('front.register');

        Route::post('/doLogin', 'AuthController@doLogin')->name('front.doLogin');

        Route::get('/about-us', 'FrontModuleController@about')->name('front.about');
        Route::get('/Technicians', 'FrontModuleController@technicians')->name('front.technicians');
        Route::get('/contact-us', 'FrontModuleController@contact_us')->name('front.contact_us');
        Route::post('/contact-us', 'FrontModuleController@contact')->name('front.contact');
//    Route::get('/blog-categories', 'FrontModuleController@blog_categories')->name('front.blog_categories');
//    Route::get('/blog-categories/show/{id}', 'FrontModuleController@blog_categories_show')->name('front.blog_categories_show');
        Route::get('/blogs', 'FrontModuleController@blogs')->name('front.blogs');
        Route::get('/blogs/{id}', 'FrontModuleController@blogs_show')->name('front.blogs_show');
    });

    Route::group(['middleware' => 'auth:web', 'prefix' => '', 'name' => ''], function () {

        Route::get('/logout', 'AuthController@logout')->name('front.logout');
        Route::get('/account', 'AuthController@account')->name('front.account');
        Route::get('/assign/servers', 'AuthController@assign')->name('front.assign');
        Route::post('/assign/servers', 'AuthController@assignServersToUsers')->name('front.assignToUser');
        Route::get('/assign-server-to-user/{id}', 'AuthController@assignToUser')->name('front.assignUser');
        Route::post('/assign-server-to-user', 'AuthController@assignedToUser')->name('front.assignedUser');

        Route::post('/change-server-type', 'FrontModuleController@passwordShow')->name('front.password_show');

        Route::get('/free-server', 'FrontModuleController@freeServer')->name('front.freeServer');


    });
});







