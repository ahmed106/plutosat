<?php

namespace Modules\FrontModule\Helper;

use Modules\ConfigModule\Entities\Config;

class ShareData
{
    public static function getconfig()
    {

        $configArr = [];
        $all = Config::all();
        foreach ($all as $item) {

            if ($item->is_static == 1) {
                $configArr[$item->var] = $item->static_value;

            } else {
                $configArr[$item->var] = $item->value;
            }

        }

        return $configArr;
    }


}
