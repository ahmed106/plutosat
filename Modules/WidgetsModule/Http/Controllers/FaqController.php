<?php

namespace Modules\WidgetsModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\WidgetsModule\Repository\FaqRepository;


class FaqController extends Controller
{
   
	
        private $faqRepo;
    
        public function __construct(FaqRepository $faqRepo) {
            $this->faqRepo = $faqRepo;
        } 
    
    
    public function index()
    {

		$faq = $this->faqRepo->findAll();

        return view('widgetsmodule::faq.index',compact('faq'));
    }

    
    public function create()
    {
        return view('widgetsmodule::faq.create');
    }

  
    public function store(Request $request)
    {
       $data = $request->except(['_token']);
   
        $this->faqRepo->save($data);
        
		return redirect('admin-panel/widgets/faq/')->with('success', 'success');


    }

  

    public function edit($id)
    {



        $faq = $this->faqRepo->find($id);

        

        return view('widgetsmodule::faq.edit', ['faq' => $faq]);
        
        
    }

  
    public function update(Request $request, $id)
    {
        
		$page = $request->except('_method', '_token', 'photo', 'ar', 'en', 'de');
		$pageTrans = $request->only('ar', 'en', 'de');


		$this->faqRepo->update($id, $page, $pageTrans);

		return redirect('admin-panel/widgets/faq')->with('updated', 'updated');
    }

   
    public function destroy($id)
    {
        $page = $this->faqRepo->find($id);

		$this->faqRepo->delete($page);

		return redirect()->back();
    }
}
