<?php

namespace Modules\WidgetsModule\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\UploaderHelper;
use Modules\WidgetsModule\Repository\SliderRepository;

class SlidersController extends Controller
{

    use UploaderHelper;

    private $sliderRepo;

    public function __construct(SliderRepository $sliderRepo)
    {
        $this->sliderRepo = $sliderRepo;
    }


    public function index()
    {
        $sliders = $this->sliderRepo->findAll();

        return view('widgetsmodule::Slider.index', ['sliders' => $sliders]);
    }


    public function create()
    {
        return view('widgetsmodule::Slider.create');
    }


    public function store(Request $request)
    {
        $sliderData = $request->except('_token', 'photo');
        $sliderData['created_by'] = auth()->user()->id;

        if ($request->hasFile('photo')) {
            $image = $request->file('photo');
            $imageName = $this->upload($image, 'sliders');
            $sliderData['photo'] = $imageName;
        }

        $this->sliderRepo->save($sliderData);

        return redirect('admin-panel/widgets/slider/')->with('success', 'success');
    }


    public function edit($id)
    {
        $slider = $this->sliderRepo->find($id);

        return view('widgetsmodule::Slider.edit', ['slider' => $slider]);
    }


    public function update(Request $request, $id)
    {

        $sliderPic = $this->sliderRepo->find($id);
        $slider = $request->except('_method', '_token', 'photo', 'ar', 'en', 'fr');
        $sliderTrans = $request->only('ar', 'en', 'fr');

        if ($request->hasFile('photo')) {

            $oldPic = public_path() . '/images/sliders/' . $sliderPic->photo;
            File::delete($oldPic);


            $image = $request->file('photo');
            $imageName = $this->upload($image, 'sliders');
            $slider['photo'] = $imageName;
        }

        $this->sliderRepo->update($id, $slider, $sliderTrans);

        return redirect('admin-panel/widgets/slider')->with('updated', 'updated');
    }


    public function destroy($id)
    {
        $slider = $this->sliderRepo->find($id);
        $this->sliderRepo->delete($slider);

        return redirect()->back();
    }
}
