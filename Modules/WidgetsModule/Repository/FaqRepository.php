<?php

namespace Modules\WidgetsModule\Repository;

use File;
use Modules\WidgetsModule\Entities\Faq;


class FaqRepository{
	
	public function find($id) {
		$faq = Faq::where('id', $id)->first();

		return $faq;
	}

	public function findAll() {
		$faq = Faq::with(['translations'])->get();

		return $faq;
	}

	public function save($faqData) {
		
		$faq = Faq::create($faqData);

		return $faq;
	}

	public function findLastByLimit($limit) {
		return Faq::limit($limit)->get();
	}

	public function delete($faq) {
		Faq::destroy($faq->id);
	}

	public function update($id, $data, $data_trans) {
		$faq = Faq::find($id);
		$faq->update($data);

		foreach (\LanguageHelper::getLang() as $lang) {


			if ($faq->hasTranslation('' . $lang->lang)) {
			} else {
				$faq->translateOrNew('' . $lang->lang);
			}

			$faq->translate('' . $lang->lang)->answer = $data_trans[$lang->lang]['answer'];
			$faq->translate('' . $lang->lang)->question = $data_trans[$lang->lang]['question'];
		

			$faq->save();
		}
		return $faq;

	}
}
