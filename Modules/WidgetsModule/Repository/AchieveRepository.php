<?php

namespace Modules\WidgetsModule\Repository;

use File;
use Modules\WidgetsModule\Entities\Achieve;

/**
 * SliderRepository Class, will deal with all data of Slider,
 * Including its images and relations.
 */
class AchieveRepository
{

    public function find($id)
    {
        $acheive = Achieve::where('id', $id)->first();

        return $acheive;
    }

    public function findAll()
    {
        $acheive = Achieve::with('translations', 'translation')->get()->take(3);

        return $acheive;
    }

    public function save($data)
    {
        $acheive = Achieve::create($data);
        return $acheive;
    }

    public function delete($acheive)
    {
        if ($acheive->icon) {
            $file_path = public_path() . '/images/acheives/' . $acheive->icon;

            File::delete($file_path);
        }

        Achieve::destroy($acheive->id);
    }

    public function update($id, $data, $data_trans)
    {
        $acheive = Achieve::find($id);
        $acheive->update($data);

        foreach (\LanguageHelper::getLang() as $lang) {
            $acheive->translate('' . $lang->lang)->title = $data_trans[$lang->lang]['title'];
            $acheive->translate('' . $lang->lang)->content = $data_trans[$lang->lang]['content'];

            $acheive->save();
        }
        return $acheive;
    }
}
