<?php

namespace Modules\WidgetsModule\Entities\Slider;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Slider extends Model implements TranslatableContract
{
    use Translatable;

    protected $table = 'sliders';
    protected $fillable = ['photo', 'created_by','id'];
    public $translatedAttributes = ['title', 'description'];
    public $translationModel = SliderTranslation::class;
}
