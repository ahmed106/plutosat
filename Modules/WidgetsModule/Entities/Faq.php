<?php

namespace Modules\WidgetsModule\Entities;

use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class Faq extends Model implements TranslatableContract
{
    protected $fillable = [];

    use Translatable;
    
    public $translatedAttributes = ['question', 'answer'];
  
}
