<?php

namespace Modules\WidgetsModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class WhyUs extends Model implements TranslatableContract
{
	use Translatable;

	protected $table = 'why_us';
    public $translatedAttributes = ['title', 'content'];
    protected $fillable = ['photo'];
    public $translationModel = WhyUsTranslation::class;
}
