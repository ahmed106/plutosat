<?php

namespace Modules\WidgetsModule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Achieve extends Model
{
    use \Astrotomic\Translatable\Translatable;

    protected $table = 'achieve';
    protected $fillable = ['icon', 'number'];
    public $translatedAttributes = ['title', 'content'];
    public $translationModel = AchieveTranslation::class;

}
