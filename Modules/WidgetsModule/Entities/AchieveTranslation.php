<?php

namespace Modules\WidgetsModule\Entities;

use Illuminate\Database\Eloquent\Model;

class achieveTranslation extends Model
{
    protected $fillable = ['title', 'number', 'content'];
    public $timestamps = false;
    protected $table = 'achieve_translation';
}
