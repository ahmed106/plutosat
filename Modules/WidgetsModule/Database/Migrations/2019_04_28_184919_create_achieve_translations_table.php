<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchieveTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achieve_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('achieve_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['achieve_id', 'locale']);
            $table->foreign('achieve_id')->references('id')->on('achieve')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achieve_translation');
    }
}
