<?php

namespace Modules\WidgetsModule\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\WidgetsModule\Entities\Page;

class PageSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = new Page();
        $pages->photo = public_path('images/default.jpg');
        $pages->save();
        $pages->translation()->create([
            'title' => 'who we are',
            'content' => 'Hello To Who We Are page ',
            'page_id' => $pages->id,
            'locale' => 'en',

        ]);
        $pages->translation()->create([
            'title' => 'who we are',
            'content' => 'Hello To Who We Are page ',
            'page_id' => $pages->id,
            'locale' => 'ar',

        ]);
        $pages->translation()->create([
            'title' => 'who we are',
            'content' => 'Hello To Who We Are page ',
            'page_id' => $pages->id,
            'locale' => 'fr',

        ]);

        $pages = new Page();
        $pages->photo = public_path('images/default.jpg');
        $pages->save();
        $pages->translation()->create([
            'title' => 'technicians',
            'content' => 'Hello To technicians page ',
            'page_id' => $pages->id,
            'locale' => 'en',

        ]);
        $pages->translation()->create([
            'title' => 'technicians',
            'content' => 'Hello To technicians page ',
            'page_id' => $pages->id,
            'locale' => 'ar',

        ]);
        $pages->translation()->create([
            'title' => 'technicians',
            'content' => 'Hello To technicians page ',
            'page_id' => $pages->id,
            'locale' => 'fr',

        ]);
    }
}
