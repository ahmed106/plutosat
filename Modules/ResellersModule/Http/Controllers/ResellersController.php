<?php

namespace Modules\ResellersModule\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\AreaModule\Entities\Country;
use Modules\ResellersModule\Entities\Reseller;
use Modules\ResellersModule\Entities\ResellerServer;
use Modules\ServerModule\Entities\Server;
use Modules\UserModule\Entities\User;
use Modules\UserModule\Entities\UserServer;

class ResellersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }//end function

    public function index()
    {


        $user_servers = UserServer::first();
        $now = Carbon::now();


        $resellers = User::reseller()->with('resellers')->get();


        return view('resellersmodule::resellers.index', compact('resellers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $countries = Country::all();

        return view('resellersmodule::resellers.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [

            'email' => [Rule::unique('users', 'email'), 'required'],
            'name' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $data = $request->except(['_token', 'password']);
        if ($request->password) {
            $password = Hash::make($request->password);
            $data['password'] = $password;
        }
        $user = new \Modules\UserModule\Entities\User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->type = 'reseller';
        $user->country_id = $data['country_id'];
        $user->save();
        $data['user_id'] = $user->id;
        $reseller = $user->resellers()->create(['user_id' => $user->id]);

        return redirect()->route('resellers.index')->with('success', 'data stored');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $servers = Server::with('servers')->where('is_booked', 0)->get();
        $reseller = Reseller::with('user')->where('user_id', $id)->first();


        return view('resellersmodule::resellers.show', compact('servers', 'reseller'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $countries = Country::all();

        $reseller = User::with('resellers')->find($id);
        return view('resellersmodule::resellers.edit', compact('reseller', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {

        $data = $request->except('_token', '_method');


        $user = User::with('resellers')->find($id);
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [

            'email' => [Rule::unique('users', 'email')->ignore($user->email, 'email'), 'required'],
            'name' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        if ($request->has('password') && $request->password != null) {
            $user->update(['password' => Hash::make($request->password)]);
        }
        $user_info = $user->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'country_id' => $data['country_id'],

        ]);


        return redirect()->route('resellers.index')->with('updated', 'data updated');


    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $reseller = Reseller::with('user')->where('user_id', $id)->first();
        $reseller_servers = ResellerServer::where('reseller_id', $reseller->id)->get();


        if ($reseller_servers) {
            foreach ($reseller_servers as $reseller_server) {
                $server = Server::where('id', $reseller_server->server_id)->first();
                $server->update(['is_booked' => 0]);
                $reseller_server->delete();
            }

        }


        $reseller->user()->delete();
        $reseller->delete();
        return redirect()->route('resellers.index')->with('deleted', 'data deleted');

    }

    public function assignServerToReseller(Request $request)
    {


        $reseller_id = Reseller::where('id', $request->reseller_id)->first()->id;

        $server_from = Server::where('id', $request->from)->first();

        $server_to = Server::where('id', $request->to)->first();


        $servers = Server::where('is_booked', 0)->count();


        if ($servers > 0 && $server_from != null && $server_to != null) {
            for ($server_from->id; $server_from->id <= $server_to->id; $server_from->id++) {

                $reseller_server = ResellerServer::where('server_id', $server_from->id)->first();


                if ($reseller_server) {

                    return redirect()->back()->with('error', 'Server Number ' . $reseller_server->server_id . ' Is Already Booked');
                }


                ResellerServer::create(
                    [
                        'reseller_id' => $reseller_id,
                        'server_id' => $server_from->id,
                    ]
                );


                Server::where('id', $server_from->id)->update(['is_booked' => 1]);

            }
            return redirect()->route('resellers.index')->with('success', 'data stored');
        } else {
            return redirect()->back()->with('error', 'Not Available Server');
        }


    }//end function

    public function deleteResellerServer($server_id)
    {

        $server = ResellerServer::where('server_id', $server_id)->first();
        Server::where('id', $server_id)->first()->update(['is_booked' => 0]);

        $user_servers = UserServer::where('server_id', $server_id)->first();
        $server->delete();
        if ($user_servers) {

            $user_servers->delete();
        }
        return redirect()->back();

    }//end function

    public function assignToUser($server_id, $reseller_id)

    {

        $server = ResellerServer::where('server_id', $server_id)->first();
        $users = User::where('type', 'user')->get();
        $userServers = UserServer::get();


        return view('resellersmodule::resellers.assign_to_user', compact('server_id', 'reseller_id', 'users', 'userServers', 'server'));

    }//end function

    public function assignedToUser(Request $request)
    {


        $user_id = $request->user_id;
        $server_id = $request->server_id;
        $reseller_id = $request->reseller_id;
        $expiry_date = $request->expiry_date;
        $note = $request->note;

        $userServers = UserServer::where('server_id', $server_id)->first();
        if ($userServers != null) {

            return redirect()->back()->with('error', 'server is assigned before');
        }
        UserServer::create([
            'user_id' => $user_id,
            'server_id' => $server_id,
            'reseller_id' => $reseller_id,
            'expiry_date' => $expiry_date,

        ]);
        Server::where('id', $server_id)->update(['notes' => $note]);
        ResellerServer::where('server_id', $server_id)->update(['is_booked' => 1]);
        return redirect()->back()->with('success', 'data stored');

//        $user_id = $request->user_id;
//
//        $server_from = Server::where('id', $request->from)->first();
//        $server_to = Server::where('id', $request->to)->first();
//        $servers = ResellerServer::where('is_booked', 0)->count();
//
//        if ($servers > 0 && $server_from != null && $server_to != null) {
//            for ($server_from->id; $server_from->id <= $server_to->id; $server_from->id++) {
//
//                $userServer = UserServer::where('server_id', $server_from->id)->first();
//
//                if ($userServer) {
//
//                    return redirect()->back()->with('error', 'Server Number ' . $userServer->id . ' Is Already Booked');
//                }
//
//
//                UserServer::create(
//                    [
//                        'user_id' => $user_id,
//                        'server_id' => $server_from->id,
//                    ]
//                );
//                ResellerServer::where('server_id', $server_from->id)->update(['is_booked' => 1]);
//
//            }
//            return redirect()->back()->with('success', 'data stored');
//        } else {
//            return redirect()->back()->with('error', 'Not Available Server');
//        }

    }//end function

    public function changeApproval(Request $request)
    {
        $reseller_id = $request->reseller_id;
        $approval = $request->approval;

        $reseller = User::find($reseller_id);

        if ($approval == 1) {
            $reseller->update([
                'approval' => 0,
            ]);

        } elseif ($approval == 0) {

            $reseller->update(['approval' => 1]);
        }


        return redirect()->back();
    }//end function


}
