<?php

namespace Modules\ResellersModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\ServerModule\Entities\Server;

class ResellerServer extends Model
{
    protected $fillable = ['reseller_id', 'server_id', 'is_booked'];


    public function reseller()
    {
        return $this->belongsTo(Reseller::class, 'reseller_id');

    }

    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');

    }//end function
}
