<?php

namespace Modules\ResellersModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\UserModule\Entities\User;

class Reseller extends Model
{
    protected $guarded = [];

    public function servers()
    {
        return $this->hasMany(ResellerServer::class)->where('reseller_id', $this->id);

    }//end function

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }//end function

}
