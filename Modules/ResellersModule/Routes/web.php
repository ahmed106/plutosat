<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth:admin', 'prefix' => 'admin-panel', 'name' => ''], function () {

    Route::post('resellers/assign-servers', 'ResellersController@assignServerToReseller')->name('resellers.assign-server');

    Route::delete('resellers/delete/server/{id}', 'ResellersController@deleteResellerServer')->name('resellers.deleteServer');
    Route::get('resellers/assign-to-user/{id}/reseller_id/{reseller_id}', 'ResellersController@assignToUser')->name('reseller.assign_to_user');
    Route::post('resellers/assign-to-user', 'ResellersController@assignedToUser')->name('resellers.assignedToUser');

    Route::post('resellers/change/approval', 'ResellersController@changeApproval')->name('resellers.changeApproval');

    Route::resource('resellers', 'ResellersController');

});
