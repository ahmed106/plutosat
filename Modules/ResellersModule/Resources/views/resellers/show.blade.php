@extends('commonmodule::layouts.master')

@section('title')
    Reseller
@endsection

@section('css')

@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            {{\Illuminate\Support\Str::ucfirst($reseller->user->name) }}
        </h1>

    </section>
@endsection

@section('content')


    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> Assign Servers To {{\Illuminate\Support\Str::ucfirst($reseller->name) }}</h3><br><br>


                @if($servers->count() >0)
                    <div class="text-success"> Available Servers Number: [
                        @foreach($servers  as $server)


                            <a href="{{route('servers.edit',$server->id)}}">{{$server->id}}</a> ,
                        @endforeach
                        {{--End Foreach--}}
                        ]
                    </div>
                @else
                    <span class="text-danger">No Available Servers Yet , All Servers Maybe Booked</span>
                @endif
                {{--End If--}}


            </div>


            @if ($errors->any())
                <div class="alert alert-danger ">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif

            @if(session('error'))

                <div class="alert alert-danger text-center">
                    {{session('error')}}
                </div>
            @endif
            {{--End If--}}


            <form class="form-horizontal" action="{{route('resellers.assign-server')}}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" value="{{$reseller->id}}" name="reseller_id">


                <div class="box-body">
                    <div class="col-md-9">
                        <div class="nav-tabs-custom">


                            <div class="tab-content">


                                <div class="tab-pane active" id="">


                                    <div class="row ">
                                        <div class="col-sm-8">
                                            <div class="col-md-3">
                                                <div class="form-group">

                                                    <label for="" class="col-sm-1">From</label>
                                                    <div>
                                                        <div class="col-sm-12">
                                                            <input required @if($servers->count() <=0) disabled @endif type="number" autocomplete="off" value="" class="form-control" name="from">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="form-group">

                                                    <label for=" " class="col-sm-1">To</label>
                                                    <div>
                                                        <div class="col-sm-12">
                                                            <input required @if($servers->count() <=0) disabled @endif type="number" autocomplete="off" class="form-control" name="to">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>

                            </div>


                        </div>
                    </div>


                </div>
                <div class=" box-footer">
                    <button type="submit" class="btn btn-primary pull-right">
                        Save
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
            <div class="box-body">
                <table id="adminsTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>

                        <th>Server ID</th>
                        <th>Operations</th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach ($reseller->servers as $index => $server)

                        <tr>

                            <td><a href="{{route('servers.edit',$server->server_id)}}">{{$server->server_id}}</a></td>


                            <td>

                                <a title="Edit" href="{{url('admin-panel/servers/' . $server->server_id . '/edit')}}" type="button" class="btn btn-primary">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>


                                <form class="inline" action="{{route('resellers.deleteServer' , $server->server_id)}}" method="POST">
                                    {{ method_field('DELETE') }} {!! csrf_field() !!}
                                    <button title="Delete" type="submit" onclick="return confirm('Are you sure, You want to delete Admin Data?')" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                                <a title="assign to users" href="{{route('reseller.assign_to_user' , [$server->server_id,$reseller->id] )}}" type="button" class="btn btn-warning">
                                    <i class="fa fa-plane" aria-hidden="true"></i>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>

    </section>
@endsection

@section('javascript')

    @include('commonmodule::includes.swal')

@endsection
