@extends('commonmodule::layouts.master')

@section('title')
    Assign servers   to Users
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            Assign servers to Users
        </h1>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">

                            {{--End Foreach--}}
                        </h3>
                        {{-- Add New--}}

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">

                        @if(session('error'))

                            <div class="alert alert-danger text-center">
                                {{session('error')}}
                            </div>
                        @endif
                        <table id="adminsTable" class="table table-bordered table-hover text-center">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>UserName</th>

                                <th>Assigning</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $index => $user)
                                <tr>
                                    <td> {{$index+1}} </td>


                                    <td><a href="{{route('users.show',$user->id)}}">{{$user->name}}</a></td>


                                    <td>

                                        <form action="{{route('resellers.assignedToUser')}}" method="post" id="form_">
                                            @csrf
                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                            <input type="hidden" value="{{$server->server_id}}" name="server_id">
                                            <input type="hidden" value="{{$reseller_id}}" name="reseller_id">
                                            {{--                                            <label for="">from :</label>--}}
                                            {{--                                            <input type="number" name="from" id="from">--}}

                                            {{--                                            <label for="">To :</label>--}}
                                            {{--                                            <input type="number" name="to" id="to">--}}


                                            <input type="hidden" id="expiry_date" name="expiry_date">
                                            <input type="hidden" id="note" name="note">
                                            <button title="Assign" id="assign_form" data-toggle="modal" type="submit" data-target="#exampleModal" class="btn btn-success assign_form "><i class="fa fa-plane">Assign</i></button>


                                        </form>


                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    {{--start modal--}}

    <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Expiry date</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="date" id="expiry_date_modal" name="expiry_date" class="form-control">
                        <br>
                        <label class="control-label" for="">Write Note</label>
                        <textarea class="textarea" name="note" id="note_modal" cols="75" rows="10"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save_form">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        {{--end modal--}}
    </section>
@endsection

@section('javascript') {{-- sweet alert --}}

@include('commonmodule::includes.swal')

<!-- DataTables -->
<script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/ckeditor/ckeditor.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#adminsTable').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'language': {!! yajra_lang() !!}
        });
    })

</script>
<script>
    $(document).ready(function () {

        $('.assign_form').on('click', function (e) {
            e.preventDefault();
            $('#expiry_date_modal').on('change', function () {
                var _input = $('.assign_form').parent().find('#expiry_date');
                console.log(_input)
                _input.val($(this).val());
                console.log(_input.val())


            })

            $('#note_modal').on('change', function () {
                var _note = $('.assign_form').parent().find('#note');


                console.log($(this).val())
                _note.val($(this).val());


            })


        })
        $('#save_form').on('click', function () {
            $('#form_').submit();
        })

    })
</script>

@endsection
