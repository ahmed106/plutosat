@extends('commonmodule::layouts.master')

@section('title')
    Resellers
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content-header')
    <section class="content-header">
        <h1> Resellers </h1>
    </section>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Resellers</h3>
                        {{-- Add New--}}
                        <a href="{{url('admin-panel/resellers/create')}}" type="button" class="btn btn-success pull-right">
                            <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{ trans('adminmodule::admin.add_new') }}
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="resellersTable" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>UserName</th>
                                <th>Email</th>
                                <th>Approval</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($resellers as $index => $reseller)
                                <tr>
                                    <td> {{$index+1}} </td>


                                    <td> {{$reseller->name}} </td>
                                    <td> {{$reseller->email}} </td>
                                    <td>
                                        <form class="form_approval" action="{{route('resellers.changeApproval')}}" method="post">
                                            {!! csrf_field() !!}

                                            <input type="hidden" name="reseller_id" value="{{$reseller->id}}">
                                            <input type="hidden" name="approval" value="{{$reseller->approval}}">
                                            <input type="checkbox" name="approval" value="{{$reseller->approval}}" class="checkbox approval" {{($reseller->approval==1)?'checked':''}}>

                                        </form>
                                    </td>

                                    <td>

                                        <a title="Edit" href="{{url('admin-panel/resellers/' . $reseller->id . '/edit')}}" type="button" class="btn btn-primary">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>

                                        <a title="Edit" href="{{url('admin-panel/resellers/' . $reseller->id )}}" type="button" class="btn btn-warning">
                                            <i class="fa fa-server" aria-hidden="true"></i>
                                        </a>

                                        <form class="inline" action="{{url('admin-panel/resellers/' . $reseller->id)}}" method="POST">
                                            {{ method_field('DELETE') }} {!! csrf_field() !!}
                                            <button title="Delete" type="submit" onclick="return confirm('Are you sure, You want to delete Admin Data?')" type="button" class="btn btn-danger">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('javascript') {{-- sweet alert --}}

@include('commonmodule::includes.swal')

<!-- DataTables -->
<script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#adminsTable').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'language': {!! yajra_lang() !!}
        });


    })

</script>

<script>
    $('.approval').on('click', function () {
        var td = $(this).parent().parent().find('.form_approval').submit()


    })
</script>

@endsection
