@extends('commonmodule::layouts.master')

@section('title')
    Users
@endsection

@section('css')

@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            {{\Illuminate\Support\Str::ucfirst($user->name) }}
        </h1>

    </section>
@endsection

@section('content')


    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> Assign Servers To {{\Illuminate\Support\Str::ucfirst($user->name) }}</h3><br><br>


            </div>


            @if ($errors->any())
                <div class="alert alert-danger ">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif

            @if(session('error'))

                <div class="alert alert-danger text-center">
                    {{session('error')}}
                </div>
            @endif
            {{--End If--}}


            {{--            <form class="form-horizontal" action="{{route('users.assign-server')}}" method="POST">--}}
            {{--                {{ csrf_field() }}--}}

            {{--                <input type="hidden" value="{{$user->id}}" name="user_id">--}}


            {{--                <div class="box-body">--}}
            {{--                    <div class="col-md-9">--}}
            {{--                        <div class="nav-tabs-custom">--}}


            {{--                            <div class="tab-content">--}}


            {{--                                <div class="tab-pane active" id="">--}}


            {{--                                    <div class="row ">--}}
            {{--                                        <div class="col-sm-8">--}}
            {{--                                            <div class="col-md-3">--}}
            {{--                                                <div class="form-group">--}}

            {{--                                                    <label for="" class="col-sm-1">From</label>--}}
            {{--                                                    <div>--}}
            {{--                                                        <div class="col-sm-12">--}}
            {{--                                                            <input required @if($servers->count() <=0) disabled @endif type="number" autocomplete="off" value="" class="form-control" name="from">--}}
            {{--                                                        </div>--}}
            {{--                                                    </div>--}}
            {{--                                                </div>--}}
            {{--                                            </div>--}}


            {{--                                            <div class="col-md-3">--}}
            {{--                                                <div class="form-group">--}}

            {{--                                                    <label for=" " class="col-sm-1">To</label>--}}
            {{--                                                    <div>--}}
            {{--                                                        <div class="col-sm-12">--}}
            {{--                                                            <input required @if($servers->count() <=0) disabled @endif type="number" autocomplete="off" class="form-control" name="to">--}}
            {{--                                                        </div>--}}
            {{--                                                    </div>--}}
            {{--                                                </div>--}}
            {{--                                            </div>--}}
            {{--                                        </div>--}}

            {{--                                    </div>--}}


            {{--                                </div>--}}

            {{--                            </div>--}}


            {{--                        </div>--}}
            {{--                    </div>--}}


            {{--                </div>--}}
            {{--                <div class=" box-footer">--}}
            {{--                    <button type="submit" class="btn btn-primary pull-right">--}}
            {{--                        Save--}}
            {{--                        <i class="fa fa-save"></i>--}}
            {{--                    </button>--}}
            {{--                </div>--}}
            {{--            </form>--}}
            <div class="box-body">
                <table id="adminsTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>

                        <th>Server ID</th>
                        <th>Expiry Date</th>
                        <th>Operations</th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach ($user->servers as $index => $server)


                        <tr>

                            <td><a href="{{route('servers.edit',$server->server_id)}}">{{$server->server_id}}</a></td>
                            <td>
                                <input type="hidden" name="server_id" id="server_id" value="{{$server->id}}">
                                {{$server->expiry_date}}
                                <a id="edit_modal" class="edit_modal" style="margin-left: 5px;" href=""><i class="fa fa-edit"></i></a>
                            </td>


                            <td>

                                <a title="Edit" href="{{url('admin-panel/servers/' . $server->server_id . '/edit')}}" type="button" class="btn btn-primary">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>

                                <form class="inline" action="{{route('users.deleteServer' , $server->server_id)}}" method="POST">
                                    {{ method_field('DELETE') }} {!! csrf_field() !!}
                                    <button title="Delete" type="submit" onclick="return confirm('Are you sure, You want to delete Admin Data?')" type="button" class="btn btn-danger">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </td>

                        </tr>
                    @endforeach

                    {{--End If--}}

                    </tbody>
                </table>
            </div>

        </div>

    {{--start modal--}}

    <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Expiry date</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="date" value="" id="expiry_date_modal" name="expiry_date" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="save_form">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        {{--end modal--}}

    </section>
@endsection

@section('javascript')

    @include('commonmodule::includes.swal')


    <script>
        $('.edit_modal').on('click', function (e) {

            server_id = $(this).parent().find('#server_id').val();

            e.preventDefault();
            $('#exampleModal').modal().show;

            $('#save_form').on('click', function () {
                var expiry_date = $('#expiry_date_modal').val();

                $.ajax({
                    'type': 'post',
                    'url': '{{route('users.expiryDate')}}',

                    data: {
                        '_token': '{{csrf_token()}}',
                        'server_id': server_id,
                        'expiry_date': expiry_date,
                    },
                    beforeSend: function () {

                    },
                    'statusCode': {

                        200: function (response) {
                            $('#exampleModal').modal().hide();
                            window.location.reload();

                        },
                        404: function (xhr) {
                            $.each(xhr.responseJSON.errors, function (key, value) {
                                $('#validation-errors').append('<div class="alert alert-danger">' + value + '</div');
                            });
                        }
                    }


                });
            })

        })

    </script>

@endsection
