@extends('commonmodule::layouts.master')

@section('title')
    Users
@endsection

@section('css')

@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            Users
        </h1>

    </section>
@endsection

@section('content')


    <section class="content">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> Users</h3>
            </div>


            @if ($errors->any())
                <div class="alert alert-danger ">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif

            <form class="form-horizontal" action="{{route('users.store')}}" method="POST">
                {{ csrf_field() }}

                <div class="box-body">
                    <div class="col-md-9">
                        <div class="nav-tabs-custom">


                            <div class="tab-content">


                                <div class="tab-pane active" id="">


                                    <div class="form-group">
                                        <label class="control-label col-sm-2">User Name :</label>
                                        <div class="col-sm-9">
                                            <input required type="text" autocomplete="off" value="" class="form-control" name="name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Email :</label>
                                        <div class="col-sm-9">
                                            <input required type="email" autocomplete="off" value="" class="form-control" name="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Country :</label>
                                        <div class="col-sm-9">
                                            <select name="country_id" id="country_id" class="form-control">
                                                <option selected disabled value="">Select Country</option>
                                                @foreach($countries as $country)

                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                                {{--End Foreach--}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Password :</label>
                                        <div class="col-sm-9">
                                            <input required type="password" autocomplete="off" value="" class="form-control" name="password">
                                        </div>
                                    </div>


                                </div>

                            </div>


                        </div>
                    </div>


                </div>
                <div class=" box-footer">
                    <button type="submit" class="btn btn-primary pull-right">
                        Save
                        <i class="fa fa-save"></i>
                    </button>
                </div>
            </form>
        </div>

    </section>
@endsection

@section('javascript')

    @include('commonmodule::includes.swal')

@endsection
