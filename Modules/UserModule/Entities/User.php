<?php

namespace Modules\UserModule\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\ResellersModule\Entities\Reseller;
use Modules\ServerModule\Entities\UserFreeServer;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'name', 'email', 'password', 'type', 'approval', 'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function servers()
    {
        return $this->hasMany(UserServer::class, 'user_id');

    }//end function

    public function resellers()
    {
        return $this->hasMany(Reseller::class, 'user_id');

    }//end function


    public function scopeReseller(Builder $builder)
    {
        return $builder->where('type', 'reseller');

    }//end function

    public function freeServer()
    {
        return $this->hasOne(UserFreeServer::class, 'user_id');

    }//end function

}
