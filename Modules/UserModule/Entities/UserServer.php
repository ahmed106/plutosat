<?php

namespace Modules\UserModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\ServerModule\Entities\Server;

class UserServer extends Model
{
    protected $fillable = ['user_id', 'server_id', 'reseller_id', 'name', 'email', 'expiry_date', 'expired'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');

    }

    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');

    }//end function


}
