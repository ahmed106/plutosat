<?php

namespace Modules\UserModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\AreaModule\Entities\Country;
use Modules\ResellersModule\Entities\ResellerServer;
use Modules\ServerModule\Entities\Server;
use Modules\UserModule\Entities\User;
use Modules\UserModule\Entities\UserServer;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }//end function

    public function index()
    {
        $users = DB::table('users')->where('type', 'user')->get();
        return view('usermodule::users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $countries = Country::with('translations')->get();
        return view('usermodule::users.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [

            'email' => [Rule::unique('users', 'email'), 'required'],
            'name' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }
        $data = $request->except(['_token', 'password']);
        if ($request->password) {
            $password = Hash::make($request->password);
            $data['password'] = $password;
        }

        $data['type'] = 'user';
        $user = User::create($data);

        return redirect()->route('users.index')->with('success', 'data stored');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {

        $servers = Server::with('servers')->where('is_booked', 0)->get();
        $user = User::with('servers')->find($id);


        return view('usermodule::users.show', compact('servers', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $countries = Country::with('translations')->get();
        $user = DB::table('users')->where('id', $id)->first();
        return view('usermodule::users.edit', compact('user', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {

        $data = $request->except('_token', '_method', 'password');

        $user = User::find($id);
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [

            'email' => [Rule::unique('users', 'email')->ignore($user->email, 'email'), 'required'],
            'name' => 'required',

        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors', $validator->errors());
        }

        if ($request->password && $request->password != null) {


            $user->update(['password' => Hash::make($request->password)]);


        }

        $user->update($data);
        return redirect()->route('users.index')->with('updated', 'data updated');


    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user_servers = UserServer::where('user_id', $id)->get();

        if ($user_servers) {
            foreach ($user_servers as $user_server) {

                $user_server->delete();
            }

        }

        $user->delete();
        return redirect()->route('users.index')->with('deleted', 'data deleted');

    }

    public function assignServerToUser(Request $request)
    {

        $user_id = $request->user_id;
        $server_from = Server::where('id', $request->from)->first();
        $server_to = Server::where('id', $request->to)->first();


        $servers = Server::where('is_booked', 0)->count();

        if ($servers > 0 && $server_from != null && $server_to != null) {
            for ($server_from->id; $server_from->id <= $server_to->id; $server_from->id++) {

                $userServer = UserServer::find($server_from->server_id);
                if ($userServer) {

                    return redirect()->back()->with('error', 'Server Number ' . $userServer->id . ' Is Already Booked');
                }

                UserServer::create(
                    [
                        'user_id' => $user_id,
                        'server_id' => $server_from->id,
                    ]
                );
                Server::where('id', $server_from->server_id)->update(['is_booked' => 1]);

            }
            return redirect()->route('users.index')->with('success', 'data stored');
        } else {
            return redirect()->back()->with('error', 'Not Available Server');
        }


    }//end function

    public function deleteUserServer($server_id)
    {

        $server = UserServer::where('server_id', $server_id)->first();


        $reseller_servers = ResellerServer::where('server_id', $server_id)->first();
        if ($reseller_servers) {
            $reseller_servers->update(['is_booked' => 0]);
            Server::where('id', $server_id)->update(['notes' => '']);
        }
        $server->delete();

        return redirect()->back();

    }//end function


    public function editExpiryDate(Request $request)
    {
        $server = UserServer::find($request->server_id);
        $server->update(['expiry_date' => $request->expiry_date]);

        return \response()->json(['success' => 'expiry date is updated successfully'], 200);

    }//end function

}
